package fr.afpa.cda.ecommerce.webapp.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import lombok.Data;

@Data
@PropertySource("momo-conf.properties")
@Configuration
public class MomoConfigProperties {

	@Value("${toto}")
	private String toto;
	@Value("${momo.mama.momo.mimi}")
	private String momo;
}
