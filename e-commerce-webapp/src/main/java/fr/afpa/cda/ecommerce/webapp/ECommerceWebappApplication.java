package fr.afpa.cda.ecommerce.webapp;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

import fr.afpa.cda.ecommerce.webapp.properties.StorageProperties;
import fr.afpa.cda.ecommerce.webapp.services.impl.FileSystemStorageService;

@SpringBootApplication
@EnableConfigurationProperties({ StorageProperties.class })
public class ECommerceWebappApplication {

	public static void main(String[] args) {
		SpringApplication.run(ECommerceWebappApplication.class, args);
	}

	@Bean
	CommandLineRunner init(FileSystemStorageService storageService) {
		return (args) -> {
			storageService.init();
		};
	}

}
