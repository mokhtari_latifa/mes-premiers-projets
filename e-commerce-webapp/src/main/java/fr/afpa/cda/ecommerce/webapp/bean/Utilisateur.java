package fr.afpa.cda.ecommerce.webapp.bean;

import java.nio.file.Paths;
import java.util.Date;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import fr.afpa.cda.ecommerce.webapp.controller.FileUploadController;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@SequenceGenerator(name = "utilisateur_id_seq", initialValue = 1, allocationSize = 1)
@SqlResultSetMapping(name = "StructureMapper", classes = { @ConstructorResult(targetClass = Structure.class, columns = {
		@ColumnResult(name = "nom"), @ColumnResult(name = "prenom") })

})
@NamedQueries({
		@NamedQuery(query = "select u from Utilisateur u where u.email=:login and password = :pwd", name = "requeteJPQLNommee"),
		@NamedQuery(query = "SELECT new fr.afpa.cda.ecommerce.webapp.bean.Structure(u.nom,u.prenom) FROM Utilisateur u where u.email=:login and u.password=:pwd", name = "requeteJPQLNommeeAvecNew") })
@NamedNativeQueries({
		@NamedNativeQuery(query = "SELECT * FROM utilisateur u where u.email=:login and u.password=:pwd", name = "requetNativeNommee",resultClass = Utilisateur.class),
		@NamedNativeQuery(query = "select nom,prenom FROM utilisateur u where u.email=:login and u.password=:pwd", name = "requetNativeNommeeAvecSqlResultMapping", resultSetMapping = "StructureMapper") })
public class Utilisateur {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "utilisateur_id_seq")
	private Integer id;
	private String nom;
	private String prenom;
	@Temporal(TemporalType.DATE)
	private Date dateNaissance;
	private String pathImage;
	@Email
	private String email;
	@Length
	@NotEmpty
	private String password;

	@Embedded
	private InfosContact infosContact;

	@Transient
	String url;

	public String getUrl() {
		// "/user/image/{id}/{filename:.+}"
		this.url = MvcUriComponentsBuilder.fromMethodName(FileUploadController.class, "serveProfil", this.id,
				Paths.get(this.pathImage).getFileName().toString()).build().toString();

		return this.url;
	}

}
