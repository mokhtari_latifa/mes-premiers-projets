package fr.afpa.cda.ecommerce.webapp.bean;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Structure {

	private String nom;
	private String prenom;

	public Structure(String nom, String prenom) {
		super();
		this.nom = nom;
		this.prenom = prenom;
	}

}
