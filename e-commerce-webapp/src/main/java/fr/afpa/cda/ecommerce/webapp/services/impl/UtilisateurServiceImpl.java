package fr.afpa.cda.ecommerce.webapp.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.cda.ecommerce.webapp.bean.Structure;
import fr.afpa.cda.ecommerce.webapp.bean.Utilisateur;
import fr.afpa.cda.ecommerce.webapp.dao.IUtilisateurDao;
import fr.afpa.cda.ecommerce.webapp.services.IUtilisateurService;

@Service
public class UtilisateurServiceImpl implements IUtilisateurService {

	@Autowired
	IUtilisateurDao utilisateurDao;

	@Override
	public List<Utilisateur> findAll() {
		return utilisateurDao.findAll();
	}

	@Override
	public Utilisateur findById(Integer id) {
		return utilisateurDao.getOne(id);
		// return utilisateurDao.findById(id).get();
	}

	@Override
	public Utilisateur save(Utilisateur nouveauUtilisateur) {

		return utilisateurDao.save(nouveauUtilisateur);
	}

	@Override
	public Utilisateur update(Utilisateur nouveauUtilisateur) {
		return utilisateurDao.save(nouveauUtilisateur);
	}

	@Override
	public void deleteById(Integer id) {
		utilisateurDao.deleteById(id);

	}

	@Override
	public Utilisateur findByEmailAndPassword(String login, String password) {
		Utilisateur u = utilisateurDao.seLogerDynamiqueJPQL(login, password);
		u = utilisateurDao.seLogerDynamiqueNativeSQL(login, password);
		u = utilisateurDao.seLogerNommeeJPQL(login, password);
		u = utilisateurDao.seLogerNommeeNativeSQL(login, password);
		
		Structure c = utilisateurDao.seLogerDynamiqueJPQLAvecNew(login, password);
		c = utilisateurDao.seLogerNommeeJPQLAvecNew(login, password);
		c = utilisateurDao.seLogerNommeeNativeSQLAvecSQLresultMapping(login, password);
		return u;
	}

}
