package fr.afpa.cda.ecommerce.webapp.bean;

import java.nio.file.Paths;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import fr.afpa.cda.ecommerce.webapp.controller.FileUploadController;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@SequenceGenerator(name = "produit_id_seq", initialValue = 1, allocationSize = 1)
public class Produit {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "produit_id_seq")
	private Integer id;
	private String nom;
	private String description;
	private Float poid;
	@Temporal(TemporalType.DATE)
	private Date dateFabrication;
	private String pathImage;
	@Transient
	private String url;

	public String getUrl() {
		this.url = MvcUriComponentsBuilder.fromMethodName(FileUploadController.class, "serveImage",
				Paths.get(this.pathImage).getFileName().toString()).build().toString();

		return this.url;
	}

	public Produit(Integer id, String nom, String description, Float poid, Date dateFabrication) {
		super();
		this.id = id;
		this.nom = nom;
		this.description = description;
		this.poid = poid;
		this.dateFabrication = dateFabrication;
	}

	public Produit(String nom, String description, Float poid, Date dateFabrication) {
		super();
		this.nom = nom;
		this.description = description;
		this.poid = poid;
		this.dateFabrication = dateFabrication;
	}

}
