package fr.afpa.cda.ecommerce.webapp.services;

import java.nio.file.Path;
import java.util.stream.Stream;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface IFileSystemStorageService {

	public void store(MultipartFile file);

	public Path storeAndGetPath(MultipartFile file);

	public Path storeAndGetPath(MultipartFile file, Path chemin);

	public Stream<Path> loadAll();

	public Path load(String filename);

	public Path load(String filename, String chemin);

	public Resource loadAsResource(String filename);

	public Resource loadAsResource(String filename, String chemin);

	public void deleteAll();

	public void init();
}
