package fr.afpa.cda.ecommerce.webapp.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@Data
@ConfigurationProperties("storage")
public class StorageProperties {
	
	@Value("${storage.location-dir}")
	private String mavar;

	private String locationDir;
	private String maVariable;

}
