package fr.afpa.cda.ecommerce.webapp.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.afpa.cda.ecommerce.webapp.bean.Structure;
import fr.afpa.cda.ecommerce.webapp.bean.Utilisateur;

@Repository
public interface IUtilisateurDao extends JpaRepository<Utilisateur, Integer> {

	//recherche par nom de methode
	Utilisateur findByEmailAndPassword(String login, String password);
	Utilisateur findByNomOrPrenom(String nom, String prenom);

	
	//recherche par requete directe
	
	// requete dynamique en JPQL
	@Query(value = "SELECT u FROM Utilisateur u where u.email=:login and u.password=:pwd")
	Utilisateur seLogerDynamiqueJPQL(@Param("login") String login, @Param("pwd") String password);

	@Query(value = "SELECT new fr.afpa.cda.ecommerce.webapp.bean.Structure(u.nom,u.prenom) FROM Utilisateur u where u.email=:login and u.password=:pwd")
	Structure seLogerDynamiqueJPQLAvecNew(@Param("login") String login, @Param("pwd") String password);


	@Query(value = "SELECT * FROM utilisateur u where u.email=:login and u.password=:pwd", nativeQuery = true)
	Utilisateur seLogerDynamiqueNativeSQL(@Param("login") String login, @Param("pwd") String password);

	//	@Query(value = "SELECT * FROM utilisateur u where u.email=:login and u.password=:pwd", nativeQuery = true,)
//	Structure seLogerDynamiqueNativeSQLAvecNew(@Param("login") String login, @Param("pwd") String password);

	@Query(name = "requeteJPQLNommee")
	Utilisateur seLogerNommeeJPQL(@Param("login") String login, @Param("pwd") String password);

	@Query(name = "requeteJPQLNommeeAvecNew")
	Structure seLogerNommeeJPQLAvecNew(@Param("login") String login, @Param("pwd") String password);

	@Query(name = "requetNativeNommee",nativeQuery = true)
	Utilisateur seLogerNommeeNativeSQL(@Param("login") String login, @Param("pwd") String password);

	@Query(name = "requetNativeNommeeAvecSqlResultMapping",nativeQuery = true)
	Structure seLogerNommeeNativeSQLAvecSQLresultMapping(@Param("login") String login, @Param("pwd") String password);

}
