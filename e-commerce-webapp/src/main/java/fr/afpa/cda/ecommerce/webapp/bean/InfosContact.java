package fr.afpa.cda.ecommerce.webapp.bean;

import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class InfosContact {

	private String ligne1;
	private String complement;
	private String codePostal;
	private String fix;
	private String portable;
	

}
