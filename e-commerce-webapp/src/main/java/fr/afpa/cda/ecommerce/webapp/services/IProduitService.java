package fr.afpa.cda.ecommerce.webapp.services;

import java.util.List;

import fr.afpa.cda.ecommerce.webapp.bean.Produit;

public interface IProduitService {
	public List<Produit> findAll();

	public Produit findById(Integer id);

	public Produit save(Produit nouveauProduit);

	public Produit update(Produit nouveauProduit);

	public void deleteById(Integer id);
}
