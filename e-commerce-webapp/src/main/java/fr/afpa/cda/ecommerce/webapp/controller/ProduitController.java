package fr.afpa.cda.ecommerce.webapp.controller;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.ecommerce.webapp.bean.Produit;
import fr.afpa.cda.ecommerce.webapp.services.IFileSystemStorageService;
import fr.afpa.cda.ecommerce.webapp.services.IProduitService;

/**
 * @author said
 *
 */
@Controller
public class ProduitController {
	/**
	 * 
	 */

	@Autowired
	private IProduitService produitService;

	@Autowired
	private IFileSystemStorageService fileSystemStorageService;

	@GetMapping(value = { "/", "/index", "/toto" })
	public String pageDAccueil() {
		
		return "index_page";
	}

	/**
	 * @param model
	 * @return
	 */
	@GetMapping("liste_produits")
	public String afficherListeProduits(Model model) {

		model.addAttribute("produits", produitService.findAll());
		return "liste_produits_page";
	}

	// http://localhost:9090/produits/detail?id=1
	@GetMapping("/produits/detail")
	public String afficherDetailProduitAvecParams(Model model, @RequestParam("id") Integer idProduit) {
//		public String afficherDetailProduit(Model model,@RequestParam(name="id",required = false,defaultValue = "0") Integer idProduit) {
		Produit produit = produitService.findById(idProduit);
		model.addAttribute("produit", produit);
		return "detail_produit_page";
	}

	// http://localhost:9090/produits/detail/1
	@GetMapping("/produits/detail/{id}")
	public String afficherDetailProduitAvecVariablePath(Model model, @PathVariable("id") Integer idProduit) {
		Produit produit = produitService.findById(idProduit);
		model.addAttribute("produit", produit);
		return "detail_produit_page";
	}

	// Get http://localhost:9090/produits/new
	@GetMapping("/produits/new")
	public String afficherNouveauProduit() {
		return "nouveau_produit_page";
	}

	// POST http://localhost:9090/produits/new
	@PostMapping("/produits/new")
	public String enregistrerNouveauProduitAvecParams(@RequestParam("nom") String nom,
			@RequestParam("description") String desc, @RequestParam Float poid,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam("dateFabrication") Date dateFabrication) {

		Produit nouveauProduit = new Produit( nom, desc, poid, dateFabrication);

		produitService.save(nouveauProduit);

		return "redirect:/liste_produits";

	}

	// Get http://localhost:9090/produits/new2
	@GetMapping("/produits/new2")
	public String afficherNouveauProduit2() {
		return "nouveau_produit2_page";
	}

	// POST http://localhost:9090/produits/new2
	@PostMapping("/produits/new2")
	public String enregistrerNouveauProduitAvecUnBean(Produit nouveauProduit) {

		produitService.save(nouveauProduit);

		return "redirect:/liste_produits";

	}

	// Get http://localhost:9090/produits/new2
	@GetMapping("/produits/new3")
	public String afficherNouveauProduit3() {
		return "nouveau_produit3_page";
	}

	// POST http://localhost:9090/produits/new
	@PostMapping("/produits/new3")
	public String enregistrerNouveauProduitAvecImage(@RequestParam("file_image") MultipartFile uneImage,
			Produit nouveauProduit) {

		Path path = fileSystemStorageService.storeAndGetPath(uneImage, Paths.get("produits/imgs"));

		nouveauProduit.setPathImage(path.toString());

		produitService.save(nouveauProduit);

		return "redirect:/liste_produits";

	}

}
