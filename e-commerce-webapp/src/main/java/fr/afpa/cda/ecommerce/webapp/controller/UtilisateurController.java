package fr.afpa.cda.ecommerce.webapp.controller;

import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.ecommerce.webapp.bean.InfosContact;
import fr.afpa.cda.ecommerce.webapp.bean.Utilisateur;
import fr.afpa.cda.ecommerce.webapp.services.IFileSystemStorageService;
import fr.afpa.cda.ecommerce.webapp.services.IUtilisateurService;

/**
 * @author said
 *
 */
@Controller
public class UtilisateurController {
	/**
	 * 
	 */
	@Autowired
	private IFileSystemStorageService fileSystemStorageService;
	@Autowired
	private IUtilisateurService utilisateurService;

	/**
	 * @param model
	 * @return
	 */
	@GetMapping("/profil")
	public String afficher(HttpSession session) {
		return "profil_page";
	}

	@GetMapping("/profil/inscription")
	public String afficherInscription() {
		return "nouveau_utilisateur_page";
	}

	@PostMapping("/profil/inscription")
	public String afficherDetailProduitAvecParams(@RequestParam("file_image") MultipartFile uneImage,
			Utilisateur nouveauUtilisateur, InfosContact infosContact) {

		nouveauUtilisateur.setInfosContact(infosContact);
		utilisateurService.save(nouveauUtilisateur);

		Path path = fileSystemStorageService.storeAndGetPath(uneImage,
				Paths.get("profils/" + nouveauUtilisateur.getId() + "/imgs"));
		nouveauUtilisateur.setPathImage(path.toString());

		utilisateurService.save(nouveauUtilisateur);

		return "redirect:/profil/login";
	}
	@GetMapping("/profil/login")
	public String afficherLogin(String login,String password) {
		return "login_page";
	}
	@PostMapping("/profil/login")
	public String afficherDetailProduitAvecVariablePath(HttpSession session,Model model,String login,String password) {
		Utilisateur connectedUser = utilisateurService.findByEmailAndPassword(login,password);
		
		if(connectedUser==null) {
			model.addAttribute("messageErreur", "Login/mdp invalide");
			return "login_page";
		}
		
		session.setAttribute("connectedUser", connectedUser);
		
		return "redirect:/profil";
	}
//
//	// Get http://localhost:9090/produits/new
//	@GetMapping("/profil/logout")
//	public String afficherNouveauProduit() {
//		return "nouveau_produit_page";
//	}
//
//	// POST http://localhost:9090/produits/new
//	@PostMapping("/profil/modification")
//	public String enregistrerNouveauProduitAvecParams(@RequestParam("nom") String nom,
//			@RequestParam("description") String desc, @RequestParam Float poid,
//			@DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam("dateFabrication") Date dateFabrication) {
//
//		Produit nouveauProduit = new Produit( nom, desc, poid, dateFabrication);
//
//		produitService.save(nouveauProduit);
//
//		return "redirect:/liste_produits";
//
//	}

}
