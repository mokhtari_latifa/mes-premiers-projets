package fr.afpa.cda.ecommerce.webapp.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.afpa.cda.ecommerce.webapp.bean.Produit;
import fr.afpa.cda.ecommerce.webapp.dao.IProduitDao;
import fr.afpa.cda.ecommerce.webapp.services.IProduitService;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class ProduitServiceImpl implements IProduitService {
	@Autowired
	IProduitDao produitDao;
	

	@Override
	public List<Produit> findAll() {
		return produitDao.findAll();
	}

	@Override
	public Produit findById(Integer id) {
		return produitDao.getOne(id);// <==>
	}

	@Override
	public Produit save(Produit nouveauProduit) {
		return produitDao.save(nouveauProduit);
	}

	@Override
	public Produit update(Produit nouveauProduit) {
		return produitDao.save(nouveauProduit);
	}

	@Override
	public void deleteById(Integer id) {
		produitDao.deleteById(id);
	}

}
