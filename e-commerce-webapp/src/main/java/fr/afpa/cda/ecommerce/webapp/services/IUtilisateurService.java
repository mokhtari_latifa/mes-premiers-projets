package fr.afpa.cda.ecommerce.webapp.services;

import java.util.List;

import fr.afpa.cda.ecommerce.webapp.bean.Utilisateur;

public interface IUtilisateurService {
	public List<Utilisateur> findAll();

	public Utilisateur findById(Integer id);

	public Utilisateur save(Utilisateur nouveauUtilisateur);

	public Utilisateur update(Utilisateur nouveauUtilisateur);

	public void deleteById(Integer id);

	public Utilisateur findByEmailAndPassword(String login, String password);
}
