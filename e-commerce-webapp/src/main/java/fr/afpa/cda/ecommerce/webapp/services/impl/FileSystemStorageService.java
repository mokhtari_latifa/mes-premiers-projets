package fr.afpa.cda.ecommerce.webapp.services.impl;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.ecommerce.webapp.exceptions.StorageException;
import fr.afpa.cda.ecommerce.webapp.exceptions.StorageFileNotFoundException;
import fr.afpa.cda.ecommerce.webapp.properties.StorageProperties;
import fr.afpa.cda.ecommerce.webapp.services.IFileSystemStorageService;

@Service
public class FileSystemStorageService implements IFileSystemStorageService {
	


	private final Path rootLocation;

	@Autowired
	public FileSystemStorageService(StorageProperties properties) {
		this.rootLocation = Paths.get(properties.getLocationDir());
		
	}

	@Override
	public void store(MultipartFile file) {
		String filename = StringUtils.cleanPath(file.getOriginalFilename());
		try {
			if (file.isEmpty()) {
				throw new StorageException("Failed to store empty file " + filename);
			}
			if (filename.contains("..")) {
				// This is a security check
				throw new StorageException(
						"Cannot store file with relative path outside current directory " + filename);
			}
			InputStream inputStream = file.getInputStream();
			Files.copy(inputStream, this.rootLocation.resolve(filename), StandardCopyOption.REPLACE_EXISTING);

		} catch (IOException e) {
			throw new StorageException("Failed to store file " + filename, e);
		}
	}

	@Override
	public Path storeAndGetPath(MultipartFile file) {
		return storeAndGetPath(file, null);

	}

	@Override
	public Path storeAndGetPath(MultipartFile file, Path chemin) {
		String filename = StringUtils.cleanPath(file.getOriginalFilename());
		try {
			if (file.isEmpty()) {
				throw new StorageException("Failed to store empty file " + filename);
			}
			if (filename.contains("..")) {
				// This is a security check
				throw new StorageException(
						"Cannot store file with relative path outside current directory " + filename);
			}
			InputStream inputStream = file.getInputStream();
			Path pathFinal = null;
			if (chemin == null) {
				pathFinal = this.rootLocation;
			} else {
				pathFinal = this.rootLocation.resolve(chemin);
			}
			if (Files.notExists(pathFinal)) {
				Files.createDirectories(pathFinal);
			}
			pathFinal = pathFinal.resolve(filename);
			Files.copy(inputStream, pathFinal, StandardCopyOption.REPLACE_EXISTING);
			return pathFinal;
		} catch (IOException e) {
			throw new StorageException("Failed to store file " + filename, e);
		}

	}
	
	
	
	
	@Override
	public Stream<Path> loadAll() {
		try {
			return Files.walk(this.rootLocation, 1).filter(path -> !path.equals(this.rootLocation))
					.map(this.rootLocation::relativize);
		} catch (IOException e) {
			throw new StorageException("Failed to read stored files", e);
		}

	}

	@Override
	public Path load(String filename) {
		return load(filename, null);
	}

	@Override
	public Path load(String filename, String chemin) {
		if (chemin == null || chemin.isEmpty())
			return this.rootLocation.resolve(filename);
		else
			return this.rootLocation.resolve(chemin).resolve(filename);
	}

	@Override
	public Resource loadAsResource(String filename) {
		try {
			Path file = load(filename);
			Resource resource = new UrlResource(file.toUri());
			if (resource.exists() || resource.isReadable()) {
				return resource;
			} else {
				throw new StorageFileNotFoundException("Could not read file: " + filename);

			}
		} catch (MalformedURLException e) {
			throw new StorageFileNotFoundException("Could not read file: " + filename, e);
		}
	}

	@Override
	public Resource loadAsResource(String filename, String chemin) {
		try {

			Path file = null;
			if (chemin == null || chemin.isEmpty()) {
				file = load(filename);
			} else {
				file = load(filename, chemin);
			}
			Resource resource = new UrlResource(file.toUri());
			if (resource.exists() || resource.isReadable()) {
				return resource;
			} else {
				throw new StorageFileNotFoundException("Could not read file: " + filename);

			}
		} catch (MalformedURLException e) {
			throw new StorageFileNotFoundException("Could not read file: " + filename, e);
		}
	}

	@Override
	public void deleteAll() {
		FileSystemUtils.deleteRecursively(this.rootLocation.toFile());
	}

	@Override
	public void init() {
		try {
			Files.createDirectories(this.rootLocation);
		} catch (IOException e) {
			throw new StorageException("Could not initialize storage", e);
		}
	}
}
