package fr.afpa.cda.ecommerce.webapp.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.afpa.cda.ecommerce.webapp.bean.Produit;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public interface IProduitDao extends JpaRepository<Produit, Integer> {

}
