package fr.myhttp.server.exception;

public class NotFoundFichier extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * @param message
	 */
	public NotFoundFichier(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}
	

	/**
	 * 
	 */
	public NotFoundFichier() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public NotFoundFichier(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	

}
