package fr.myhttp.server.app;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.Date;

import fr.myhttp.server.model.ClientProcessor;
import fr.myhttp.server.utils.Utile;

public class HttpServer {

	
	public static void main(String[] args) {
		String cheminConfig = "";
		if (args.length > 0) {
			cheminConfig = args[0];
		}
		try {
			Config.init(cheminConfig);
		} catch (IOException e) {
			System.out.println("Fichier non trouver!");
		}
		startServer();
	}

	/**
	 * Start le processor
	 */
	public static void startServer() {

		try {
			ServerSocket serverConnect = new ServerSocket(Integer.parseInt(Utile.getPORT()));
			System.out.println("Le serveur est d�marr�... port : " + Integer.parseInt(Utile.getPORT()) + " ...\n");

			while (true) {
				ClientProcessor myServer = new ClientProcessor(serverConnect.accept());
				Thread thread = new Thread(myServer);
				thread.start();
			}

		} catch (NullPointerException e) {
			System.err.println("request vide : " + e.getMessage());
		} catch (IOException e) {
			System.err.println("Serveur Connection error : " + e.getMessage());
		}
	}

}
