package fr.myhttp.server.app;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import fr.myhttp.server.utils.PropertiesHelper;
import fr.myhttp.server.utils.Utile;



public class Config {
	
	/**
	 * Paramétrer les constantes <br>
	 * Port et Chemin
	 * @param cheminConfig
	 * @throws IOException
	 */
	public static void init(String cheminConfig) throws IOException {
		
		Properties maConfig = PropertiesHelper.getConfigurations(cheminConfig);
		
		String chemin = maConfig.getProperty("CHEMIN");
		String port = maConfig.getProperty("PORT");
		Utile.CHEMIN = new File(chemin.replace("\"", " ").trim());
		Utile.PORT = port;
	}
}
