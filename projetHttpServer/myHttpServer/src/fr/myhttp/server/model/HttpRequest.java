package fr.myhttp.server.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import fr.myhttp.server.utils.Utile;

public class HttpRequest {

	private String protocol;
	private String methode;
	private String versionProtocol;
	private String url;

	private Map<String, String> mapsHeaders;
	private HttpBody body;

	/**
	 * 
	 */
	public HttpRequest() {
		super();
		body = new HttpBodyText();
		mapsHeaders = new HashMap<String, String>();
	}

	/**
	 * Convertir une request <br>
	 * en une chaine de caracteres
	 * @param request le server renvoie
	 * @return une chaine de caracteres
	 */
	public static String format(HttpRequest request) {
		StringBuilder builder = new StringBuilder();
		System.out.println();
		builder.append(request.getMethode()+" "+request.getUrl()+" "+request.getProtocol() + "/" + request.getVersionProtocol());
		builder.append("\n");
		builder.append(request.getMapsHeaders());
		System.out.println();
		System.out.println(builder.toString());
		return builder.toString();
	

	}

	/**
	 * Convertir une chaine de caracteres <br>
	 * en objet request 
	 * @param requestString
	 * @return
	 */
	public static HttpRequest parse(String requestString, BufferedReader in) {
		HttpRequest request = new HttpRequest();
		System.out.println("request " + requestString);
		StringTokenizer parse = new StringTokenizer(requestString, " /");
		request.setMethode(parse.nextToken().toUpperCase());
		request.setUrl(parse.nextToken().toLowerCase());
		request.setProtocol(parse.nextToken().toUpperCase());
		request.setVersionProtocol(parse.nextToken().toLowerCase());
		request.setMapsHeaders(parseHeaders(in));
		return request;
	}

	/**
	 * R�cup�rer de headers de request <br>
	 * de flux entrant
	 * 
	 * @param in flux
	 * @return map valeurs des headers
	 */
	public static Map<String, String> parseHeaders(BufferedReader in) {
		Map<String, String> mapHeader = new HashMap<String, String>();
		HttpBodyText body = new HttpBodyText();
		List<String> listeHeaders = new ArrayList<String>();
		StringTokenizer parse = null;
		try {
			String line = in.readLine();
			while (in.read() >= -1) {
				while (!line.isEmpty() && line != null) {
					parse = new StringTokenizer(line, ":");
					while (parse.hasMoreTokens()) {
						listeHeaders.add(parse.nextToken());
						System.out.println(line);
					}
					line = in.readLine();
				}
				body.setContent(in.readLine().trim());
			}
			mapHeader = Utile.listeToMap(listeHeaders);
			return mapHeader;
		} catch (IOException e) {
			System.out.println("pas de flux " + e.getMessage());
			e.printStackTrace();
		}
		return mapHeader;
	}

	/**
	 * @return the methode
	 */
	public String getMethode() {
		return methode;
	}

	/**
	 * @param methode the methode to set
	 */
	public void setMethode(String methode) {
		this.methode = methode;
	}

	/**
	 * @return the protocol
	 */
	public String getProtocol() {
		return protocol;
	}

	/**
	 * @param protocol the protocol to set
	 */
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	/**
	 * @return the versionProtocol
	 */
	public String getVersionProtocol() {
		return versionProtocol;
	}

	/**
	 * @param versionProtocol the versionProtocol to set
	 */
	public void setVersionProtocol(String versionProtocol) {
		this.versionProtocol = versionProtocol;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the body
	 */
	public HttpBody getBody() {
		return body;
	}

	/**
	 * @param body the body to set
	 */
	public void setBody(HttpBody body) {
		this.body = body;
	}

	/**
	 * @return the mapsHeaders
	 */
	public Map<String, String> getMapsHeaders() {
		return mapsHeaders;
	}

	/**
	 * @param mapsHeaders the mapsHeaders to set
	 */
	public void setMapsHeaders(Map<String, String> mapsHeaders) {
		this.mapsHeaders = mapsHeaders;
	}

}
