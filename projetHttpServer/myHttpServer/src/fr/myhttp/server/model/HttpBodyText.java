package fr.myhttp.server.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import fr.myhttp.server.exception.NotFoundFichier;

public class HttpBodyText extends HttpBody {

	private String content;

	/**
	 * Lire le fichier 
	 * @return the content de fichier
	 */
	@Override
	public String getContent(File file) throws NotFoundFichier {
		FileReader fr;
		try {
			if (!file.exists() || file == null) {
				throw new NotFoundFichier("Erreur fichier est introuvable !");
			}
			fr = new FileReader(file.getAbsoluteFile());
			BufferedReader reader = new BufferedReader(fr);
			StringBuilder stringBuilder = new StringBuilder();
			String line = null;
			String ls = System.getProperty("line.separator");

			while ((line = reader.readLine()) != null) {
				stringBuilder.append(line);
				stringBuilder.append(ls);
			}

			stringBuilder.deleteCharAt(stringBuilder.length() - 1);
			reader.close();
			String content = stringBuilder.toString();
			setContent(content);

		} catch (IOException e) {
			System.out.println("Erreur lire fichier :" + e.getMessage());
		}

		return content;
	}



	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}



	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	
	
}
