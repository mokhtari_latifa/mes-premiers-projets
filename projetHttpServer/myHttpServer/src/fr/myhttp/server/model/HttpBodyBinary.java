package fr.myhttp.server.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import fr.myhttp.server.exception.NotFoundFichier;

public class HttpBodyBinary extends HttpBody {

	private byte[] Binaycontent;

	/**
	 * @return the binaycontent
	 * @throws NotFoundFichier
	 * 
	 */
	@Override
	public byte[] getBinayContent(File file, int fileLength) {
		FileInputStream fileIn = null;
		byte[] fileData = new byte[fileLength];

		try {
			fileIn = new FileInputStream(file);
			fileIn.read(fileData);
		} catch (FileNotFoundException e) {
			System.out.println("Le fichier est introuvable!");
		} catch (IOException e) {
			System.out.println("Erreur lire fichier :" + e.getMessage());
		} finally {
			if (fileIn != null)
				try {
					fileIn.close();
				} catch (IOException e) {
					System.out.println("Erreur de fermeture de fichier : " + e.getMessage());
				}
		}

		return fileData;
	}

	
	/**
	 * @return the binaycontent
	 */
	public byte[] getBinaycontent() {
		return Binaycontent;
	}


	/**
	 * @param binaycontent the binaycontent to set
	 */
	public void setBinaycontent(byte[] binaycontent) {
		Binaycontent = binaycontent;
	}
	
	

}
