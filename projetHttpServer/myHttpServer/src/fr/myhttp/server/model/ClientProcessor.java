package fr.myhttp.server.model;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Date;

import fr.myhttp.server.exception.NotFoundFichier;

public class ClientProcessor implements Runnable {

	private Socket connect;

	public ClientProcessor(Socket connect) {
		this.connect = connect;
	}


	/**
	 * Recupere le request <br>
	 * et envoyer la response
	 */
	@Override
	public void run() {
		
		BufferedReader in = null;
		PrintWriter out = null;
		BufferedOutputStream dataOut = null;
		try {
			
			in = new BufferedReader(new InputStreamReader(connect.getInputStream()));
	      
			out = new PrintWriter(connect.getOutputStream());
			dataOut = new BufferedOutputStream(connect.getOutputStream());
		
	
			String line = in .readLine();
			HttpResponse response = HttpResponse.parse(line);
			int tailleFichier =(int)response.getFile().length();
			String builder = HttpResponse.format(response);
				out.println(builder);
				out.println(); 
				out.flush(); 
				HttpBody body = response.typeOfBody(response.getFile());
				if(body instanceof HttpBodyText) {
					dataOut.write(body.getContent(response.getFile()).getBytes(),0,tailleFichier);	
				}
				if(body instanceof HttpBodyBinary) {
					dataOut.write( ((HttpBodyBinary) body).getBinayContent(response.getFile(), tailleFichier),0,tailleFichier);	
				}
				
				dataOut.flush();
	

		} catch (NullPointerException e) {
			System.err.println("Request est vide: " + e.getMessage());	
		} catch (NotFoundFichier e) {
			System.out.println(e.getMessage());
		} catch (IOException ioe) {
			System.err.println("Request erreur : " + ioe);
		} finally {
			try {
				in.close();
				out.close();
				dataOut.close();
				connect.close();
			} catch (Exception e) {
				System.err.println("Erreur de fermeteur de flux: " + e.getMessage());
			}
		}

	}


	/**
	 * @return the connect
	 */
	public Socket getConnect() {
		return connect;
	}


	/**
	 * @param connect the connect to set
	 */
	public void setConnect(Socket connect) {
		this.connect = connect;
	}





}
