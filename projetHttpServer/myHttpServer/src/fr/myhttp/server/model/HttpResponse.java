
package fr.myhttp.server.model;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import fr.myhttp.server.utils.Utile;

public class HttpResponse {

	private Integer code;
	private String message;
	private String protocol;
	private String versionProtocol;
	private File file;

	private Map<String, String> headers;
	private HttpBody body = new HttpBodyBinary();

	/**
	 * Response constructeur
	 */
	public HttpResponse() {
		super();
		headers = new HashMap<String, String>();
	}

	/**
	 * Format a object response to string
	 * 
	 * @param response
	 * @return
	 */
	public static String format(HttpResponse response) {
		int fileLength = (int) response.getFile().length();
		StringBuilder builder = new StringBuilder();
		System.out.println();
		builder.append(response.getProtocol() + "/" + response.getVersionProtocol() + " " + response.getCode() + " "

				+ response.getMessage());
		builder.append("\n");
		builder.append("Server: Java : 1.0");
		builder.append("\n");
		builder.append("Date: " + new Date());
		builder.append("\n");
		builder.append("Content-type: " + response.getBody().getContentType(response.getFile().getAbsolutePath()));
		System.out.println();
		builder.append("\n");
		builder.append("Content-length: " + fileLength);
		return builder.toString();

	}

	/**
	 * Parse string to object response
	 * 
	 * @param requestString
	 * @return
	 * @throws IOException
	 */
	public static HttpResponse parse(String requestString) throws IOException {
		HttpResponse response = new HttpResponse();
		StringTokenizer parse = new StringTokenizer(requestString);
		String methode = parse.nextToken().toUpperCase();
		String uri = parse.nextToken().toLowerCase();
		String protocolPlusVersion = parse.nextToken().toUpperCase();
		parse = new StringTokenizer(protocolPlusVersion,"/");
		response.setProtocol(parse.nextToken().toUpperCase());
		response.setVersionProtocol(parse.nextToken().toLowerCase());
		File file = new File(Utile.getCHEMIN(), uri);
		if (!methode.equals("GET") && !methode.equals("POST")) {
			response.setCode(405);
			response.setMessage("Method Not Allowed");
			file = new File("lib\\erreur405.png");
		} else if (!file.exists()) {
			response.setCode(404);
			response.setMessage("Not Found");
			file = new File("lib\\erreur404.jpg");
		} else if (uri.endsWith("/")) {
			file = new File("lib\\home.html");
		} else {
			response.setCode(200);
			response.setMessage("Ok");
		}

		response.setFile(file);
		return response;

	}

	/**
	 * Determiner type of body
	 * 
	 * @param file demande
	 * @return instance of body
	 * @throws Exception
	 */
	public HttpBody typeOfBody(File file){
	
		try {
			if (body.isBinaryFile(file)) {
				body = new HttpBodyBinary();
				return body;
			}
		} catch (IOException e) {
			System.out.println("Erreur lire fichier "+e.getMessage());
		}
		body = new HttpBodyText();
		return body;
	}

	/**
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the protocol
	 */
	public String getProtocol() {
		return protocol;
	}

	/**
	 * @param protocol the protocol to set
	 */
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	/**
	 * @return the versionProtocol
	 */
	public String getVersionProtocol() {
		return versionProtocol;
	}

	/**
	 * @param versionProtocol the versionProtocol to set
	 */
	public void setVersionProtocol(String versionProtocol) {
		this.versionProtocol = versionProtocol;
	}

	/**
	 * @return the body
	 */
	public HttpBody getBody() {
		return body;
	}

	/**
	 * @param body the body to set
	 */
	public void setBody(HttpBody body) {
		this.body = body;
	}

	/**
	 * @return the headers
	 */
	public Map<String, String> getHeaders() {
		return headers;
	}

	/**
	 * @param headers the headers to set
	 */
	public void setHeaders(Map<String, String> headers) {
		this.headers = headers;
	}

	/**
	 * @return the file
	 */
	public File getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(File file) {
		this.file = file;
	}

}
