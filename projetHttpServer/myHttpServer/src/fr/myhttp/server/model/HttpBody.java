package fr.myhttp.server.model;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import fr.myhttp.server.exception.NotFoundFichier;

public  class HttpBody {

	
	
	public String getContent(File file) throws NotFoundFichier {
		return null;
	}
	
	public byte[] getBinayContent(File file, int fileLength) {
		return null;
	}

	/**La methode compare l'extensions de fichier <br>
	 * pour tourner le correspondant contentTpe <br>
	 * @param uri de fichier demandee
	 * @return string contentType
	 */
	public String getContentType(String uri) {
		if (uri.endsWith(".htm") || uri.endsWith(".html"))
			return "text/html";
		if (uri.endsWith(".css"))
			return "text/css";
		if (uri.endsWith(".csv"))
			return "text/csv";
		if (uri.endsWith(".jpeg") || uri.endsWith(".jpg"))
			return "image/jpeg";
		if (uri.endsWith(".gif"))
			return "image/gif";
		if (uri.endsWith(".png"))
			return "image/png";
		if (uri.endsWith(".js"))
			return "application/javascript";
		if (uri.endsWith(".json"))
			return "application/json";
		if (uri.endsWith(".pdf"))
			return "application/pdf";
		if (uri.endsWith(".jar"))
			return "application/java-archive";
		if (uri.endsWith(".tar"))
			return "application/x-tar";
		if (uri.endsWith(".svg"))
			return "image/svg+xml";
		if (uri.endsWith(".xhtml"))
			return "application/xhtml+xml";
		if (uri.endsWith(".xml"))
			return "application/xml";
		if (uri.endsWith(".mpeg"))
			return "video/mpeg";
		if (uri.endsWith(".tif") || uri.endsWith(".tiff"))
			return "image/tiff";
		else
			return "text/plain";

	}

	/**
	 * Verifier si fichier binaire ou pas 
	 * @param f  file
	 * @return boolean
	 * @throws IOException
	 */
	public  boolean isBinaryFile(File f) throws IOException {
        String type = Files.probeContentType(f.toPath());
        if (type == null) {
            return true;
        } else if (type.startsWith("text")) {
            return false;
        } else {
            return true;
        }
    }
}
