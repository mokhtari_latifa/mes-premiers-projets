package fr.myhttp.server.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesHelper {

	private static Properties configuration;

	public static Properties getConfigurations(String chemin) throws IOException {
		InputStream inputStream = null;
		if (chemin == null || chemin.trim().isEmpty()) {
			inputStream = PropertiesHelper.class.getClassLoader().getResourceAsStream("configuration.properties");
		} else {
			inputStream = new FileInputStream(chemin);
		}
		if (configuration == null)
			configuration = getPropertiesFiles(inputStream);
		return configuration;
	}

	public static Properties getConfigurations() throws IOException {
		return getConfigurations(null);
	}

	private static Properties getPropertiesFiles(InputStream inputStream) throws IOException {
		try {
			Properties prop = new Properties();

			if (inputStream != null) {
				prop.load(inputStream);
				return prop;
			} else {
				throw new FileNotFoundException("property file  not found in the classpath");
			}

		} catch (Exception e) {
			System.out.println("Exception: " + e);
			throw new FileNotFoundException("property file  not found in the classpath");
		} finally {
			if (inputStream != null)
				inputStream.close();
		}

	}
}