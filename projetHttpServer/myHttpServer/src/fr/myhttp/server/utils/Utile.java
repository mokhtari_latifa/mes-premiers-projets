package fr.myhttp.server.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Utile {

	public static File CHEMIN = null;
	public static String PORT = "8080";

	
	
	/**
	 * Verfier si le fichier est binary
	 * @param f
	 * @return
	 * @throws IOException
	 */
	public static boolean isBinaryFile(File f) throws IOException {
        String type = Files.probeContentType(f.toPath());
        if (type == null) {
            return true;
        } else if (type.startsWith("text")) {
            return false;
        } else {
            return true;
        }
    }

	/**
	 * Remplire le map header du une liste <br>
	 * @param listes de headers
	 * @return map contient des headers
	 */
	public static Map<String ,String> listeToMap(List<String> listes){
		 Map <String,String> hm = new HashMap<String,String>();		 
		  if(listes!= null && !listes.isEmpty()) {
			for (int i = 0; i < listes.size(); i=i+2) {
				hm.put(listes.get(i), listes.get(i+1));
			}
		  }
		  return hm ;
	}



	/**
	 * @return the cHEMIN
	 */
	public static File getCHEMIN() {
		return CHEMIN;
	}

	/**
	 * @return the pORT
	 */
	public static String getPORT() {
		return PORT;
	}

	/**
	 * @param cHEMIN the cHEMIN to set
	 */
	public static void setCHEMIN(File cHEMIN) {
		CHEMIN = cHEMIN;
	}

	/**
	 * @param pORT the pORT to set
	 */
	public static void setPORT(String pORT) {
		PORT = pORT;
	}




	
	
}
