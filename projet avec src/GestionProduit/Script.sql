--# fr.afpa.configuration.ConnexionFactory;

create database ma_base_latifa;

-- public.produit definition

-- Drop table

-- DROP TABLE public.produit;

CREATE TABLE public.produit (
	id numeric NOT NULL,
	nom varchar(20) NOT NULL,
	prix numeric NOT NULL,
	couleur varchar(20) NULL,
	categorie varchar(80) NULL,
	description varchar(80) NULL,
	CONSTRAINT produit_pkey PRIMARY KEY (id)
);


insert into produit Values (1,'TV LG',200,'#ff8040','cat�gorie1','T�l�viseur LCD 44 � 55');
insert into produit Values (2,'Appel iPad',140,'#808040','cat�gorie2','Tablette tactile');
insert into produit Values (3,'PC Dell',300,'#808080','cat�gorie3','Ordinateur ultra-portable');
-- public.utilisateur definition

-- Drop table

-- DROP TABLE public.utilisateur;

CREATE TABLE public.utilisateur (
	id numeric NOT NULL,
	nom varchar(20) NOT NULL,
	prenom varchar(20) NOT NULL,
	login varchar(8) NULL,
	"password" varchar(8) NOT NULL,
	CONSTRAINT utilisateur_login_key UNIQUE (login),
	CONSTRAINT utilisateur_pkey PRIMARY KEY (id)
);

insert into utilisateur Values (1,'mokhtari','latifa','login','mot');
insert into utilisateur Values (2,'momo','said','said','pass');