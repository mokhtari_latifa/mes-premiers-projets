package fr.afpa.bean;

public class Produit {
	private Integer id;
	private String nom;
	private Double prix;
	private String couleur;
	private String description;
	private String categorie;
	private Integer quantite;

	public Produit() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Produit(Integer id, String nom, Double prix, String couleur, String description, String categorie) {
		super();
		this.id = id;
		this.nom = nom;
		this.prix = prix;
		this.couleur = couleur;
		this.description = description;
		this.categorie = categorie;
	}
	public Produit( String nom, Double prix, String couleur, String description, String categorie) {
		super();
		this.id = id;
		this.nom = nom;
		this.prix = prix;
		this.couleur = couleur;
		this.description = description;
		this.categorie = categorie;
	}

	/**
	 * @return the categorie
	 */
	public String getCategorie() {
		return categorie;
	}

	/**
	 * @param categorie the categorie to set
	 */
	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the prix
	 */
	public Double getPrix() {
		return prix;
	}

	/**
	 * @param prix the prix to set
	 */
	public void setPrix(Double prix) {
		this.prix = prix;
	}

	/**
	 * @return the couleur
	 */
	public String getCouleur() {
		return couleur;
	}

	/**
	 * @param couleur the couleur to set
	 */
	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the quantite
	 */
	public Integer getQuantite() {
		return quantite;
	}

	/**
	 * @param quantite the quantite to set
	 */
	public void setQuantite(Integer quantite) {
		this.quantite = quantite;
	}
	
	

}
