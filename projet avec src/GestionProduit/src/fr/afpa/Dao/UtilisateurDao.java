package fr.afpa.Dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import fr.afpa.bean.Utilisateur;
import fr.afpa.configuration.ConnexionFactory;

public class UtilisateurDao {

	private static UtilisateurDao instance = new UtilisateurDao();

	/**
	 * 
	 */
	private UtilisateurDao() {
		super();
	}

	/**
	 * log in white
	 * 
	 * @param login
	 * @param passeWord
	 * @return Utilisateur form BDD
	 */
	public Utilisateur connextion(String login, String passWord) {
		PreparedStatement pStatement;
		Utilisateur utilisateur = null;
		try {
			pStatement = ConnexionFactory.getConnection()
					.prepareStatement("SELECT * from utilisateur where login = ? and password = ?");
			pStatement.setString(1, login);
			pStatement.setString(2, passWord);

			ResultSet monResultat = pStatement.executeQuery();
			while (monResultat.next()) {
				utilisateur = new Utilisateur();
				utilisateur.setId(monResultat.getInt("id"));
				utilisateur.setNom(monResultat.getString("nom"));
				utilisateur.setPrenom(monResultat.getString("prenom"));
				utilisateur.setLogin(monResultat.getString("login"));
				utilisateur.setPassWord(monResultat.getString("passWord"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return utilisateur;
	}

	/**
	 * insert into BDD a new utilisateur
	 * 
	 * @param joueur
	 */
	public void creatutilisateur(Utilisateur utilisateur) {
		PreparedStatement pStatement;
		Integer id = getLastId();
		id++;
		utilisateur.setId(id);

		try {
			pStatement = ConnexionFactory.getConnection()
					.prepareStatement("insert into utilisateur Values (?,?,?,?,?)");
			pStatement.setInt(1, utilisateur.getId());
			pStatement.setString(2, utilisateur.getNom());
			pStatement.setString(3, utilisateur.getPrenom());
			pStatement.setString(4, utilisateur.getLogin());
			pStatement.setString(5, utilisateur.getPassWord());
			int row = pStatement.executeUpdate();
			System.out.println(row);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	

	/**
	 * insert into BDD a new user
	 * 
	 * @param joueur
	 */
	public void creatUser(Utilisateur user) {
		PreparedStatement pStatement;
		Integer id = getLastId();
		id++;
		user.setId(id);

		try {
			pStatement = ConnexionFactory.getConnection().prepareStatement("insert into utilisateur Values (?,?,?,?,?)");
			pStatement.setInt(1, user.getId());
			pStatement.setString(2, user.getNom());
			pStatement.setString(3, user.getPrenom());
			pStatement.setString(4, user.getLogin());
			pStatement.setString(5, user.getPassWord());
			int row = pStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}


	/**
	 * get last id from BDD
	 */
	public Integer getLastId() {
		Integer idmax = null;

		PreparedStatement pStatement;
		try {
			pStatement = ConnexionFactory.getConnection().prepareStatement("select max(id ) from utilisateur");
			ResultSet monResultat = pStatement.executeQuery();
			while (monResultat.next()) {
				idmax = monResultat.getInt(1);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return idmax;
	}

	/**
	 * @return the instance
	 */
	public static UtilisateurDao getInstance() {
		return instance;
	}

}
