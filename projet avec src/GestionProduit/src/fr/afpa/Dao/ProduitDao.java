/**
 * 
 */
package fr.afpa.Dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.afpa.bean.Produit;
import fr.afpa.configuration.ConnexionFactory;

/**
 * @author elyla
 *
 */
public class ProduitDao {

	private static ProduitDao instance = new ProduitDao();

	
	
	/**
	 * 
	 */
	private ProduitDao() {
		super();
	}

	/**
	 * Creation d'un produit
	 * 
	 * @param produit
	 */
	public void save(Produit produit) {
		PreparedStatement pStatement;
		Integer id = getLastId();
		id++;
		produit.setId(id);
		try {
			pStatement = ConnexionFactory.getConnection().prepareStatement("insert into produit Values (?,?,?,?,?,?)");
			pStatement.setInt(1, produit.getId());
			pStatement.setString(2, produit.getNom());
			pStatement.setDouble(3, produit.getPrix());
			pStatement.setString(4, produit.getCouleur());
			pStatement.setString(5, produit.getCategorie());
			pStatement.setString(6, produit.getDescription());
			pStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Delete product from BDD by id
	 * @param idProduit
	 */
	public void delete(Integer idProduit) {
		PreparedStatement pStatement;
		String request = "delete from produit where id = ?";
		try {
			pStatement = ConnexionFactory.getConnection().prepareStatement(request);
			pStatement.setInt(1, idProduit);
			pStatement.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

	}
	
	/**
	 * find product by id 
	 * @param id product 
	 * @return object product form BDD
	 */
	public Produit findById(Integer id) {
		PreparedStatement statement ;
		Produit produit = null;
		String request = "select * from produit where id =?";
		try {
			statement = ConnexionFactory.getConnection().prepareStatement(request);
			statement.setInt(1, id);

			ResultSet monResultat =statement.executeQuery();
			while (monResultat.next()) {
				produit = new Produit();
				produit.setId(monResultat.getInt("id"));
				produit.setNom(monResultat.getString("nom"));
				produit.setPrix(monResultat.getDouble("prix"));
				produit.setCouleur(monResultat.getString("couleur"));
				produit.setCategorie(monResultat.getString("categorie"));
				produit.setDescription(monResultat.getString("description"));
			}
		} catch (Exception e) {
		   System.out.println(e.getMessage());
		}
		
		return produit;
	}
	
	/**
	 * Update product in BDD
	 * @param p
	 */
	public void update(Produit p) {
		PreparedStatement pStatement;
		String request = "UPDATE produit SET nom = ?, prix = ? , couleur=? , categorie= ? , description= ?  WHERE id = ? ";
		try {
			pStatement = ConnexionFactory.getConnection().prepareStatement(request);
			pStatement.setString(1,p.getNom());
			pStatement.setDouble(2,p.getPrix());
			pStatement.setString(3,p.getCouleur());
			pStatement.setString(4,p.getCategorie());
			pStatement.setString(5,p.getDescription());
			pStatement.setInt(6,p.getId());
			
			pStatement.executeUpdate();
			pStatement.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/**
	 * get last id from BDD
	 */
	public Integer getLastId() {
		Integer idmax = null;

		PreparedStatement pStatement;
		try {
			pStatement = ConnexionFactory.getConnection().prepareStatement("select max(id ) from produit");
			ResultSet monResultat = pStatement.executeQuery();
			while (monResultat.next()) {
				idmax = monResultat.getInt(1);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return idmax;
	}

	/**
	 * @return the instance
	 */
	public static ProduitDao getInstance() {
		return instance;
	}

	/**
	 * find all product fom BDD
	 * @return
	 */
	public List<Produit> findAll() {
		PreparedStatement pStatement;
		List<Produit> listProduit = new ArrayList<>();
		String request = "select * from produit ;";
		Produit produit = null;
		try {
			pStatement = ConnexionFactory.getConnection().prepareStatement(request);

			ResultSet monResultat = pStatement.executeQuery();
			while (monResultat.next()) {
				produit = new Produit();
				produit.setId(monResultat.getInt("id"));
				produit.setNom(monResultat.getString("nom"));
				produit.setPrix(monResultat.getDouble("prix"));
				produit.setCouleur(monResultat.getString("couleur"));
				produit.setCategorie(monResultat.getString("categorie"));
				produit.setDescription(monResultat.getString("description"));
				listProduit.add(produit);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return listProduit;

	}

	
}
