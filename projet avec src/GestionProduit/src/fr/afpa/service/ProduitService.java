package fr.afpa.service;

import java.util.List;

import fr.afpa.Dao.ProduitDao;
import fr.afpa.bean.Produit;

public class ProduitService {

	private static ProduitService instance = new ProduitService();

	
	
	
	/**
	 * 
	 */
	private ProduitService() {
		super();
		// TODO Auto-generated constructor stub
	}

	public List<Produit> findAll() {
		List<Produit> produits = null;
		produits = ProduitDao.getInstance().findAll();
		return produits;
		
	}

	/**
	 * find product by id
	 * @param id
	 * @return
	 */
	public  Produit chercherUnProduit(Integer id) {
		if (id != null) {
			Produit produit = ProduitDao.getInstance().findById(id);
			return produit;
		}
		return null;
	}
	
	/**
	 * save product 
	 * @param p
	 */
	public void addProduit(Produit p) {
		if (p != null) {
			ProduitDao.getInstance().save(p);
		}
	}

	
	/**
	 * delete product by id
	 * @param id
	 */
	public void supprimeProduit(Integer id) {
		if(id!= null) {
			ProduitDao.getInstance().delete(id);
		}
	}
	
	public static void update(Produit p) {
		if( p != null) {
			ProduitDao.getInstance().update(p);
		}
	}

	/**
	 * @return the instance
	 */
	public static ProduitService getInstance() {
		return instance;
	}


	
	
}
