/**
 * 
 */
package fr.afpa.service;

import fr.afpa.Dao.UtilisateurDao;
import fr.afpa.bean.Utilisateur;

/**
 * @author elyla
 *
 */
public class UtilisateurService {
	
	private static final UtilisateurService instance = new UtilisateurService();
	
	

	
	/**
	 * 
	 */
	private UtilisateurService() {
		super();
		// TODO Auto-generated constructor stub
	}


	/**
	 * la methode fait appelle � Dao <br>
	 * pour se connecter
	 * @param login
	 * @param passWord
	 * @return
	 */
	public  Utilisateur seConnecter(String login , String passWord) {
		if(login != null && passWord != null) {
			Utilisateur user = UtilisateurDao.getInstance().connextion(login, passWord);
			 return user ;
		}
		return null;
	}
	
	
	/**
	 * save product 
	 * @param p
	 */
	public void saveUser(Utilisateur user) {
		if (user != null) {
			UtilisateurDao.getInstance().creatUser(user);
		}
	}


	/**
	 * @return the instance
	 */
	public static UtilisateurService getInstance() {
		return instance;
	}
	
	
	
}
