package fr.afpa.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.bean.Panier;
import fr.afpa.bean.Produit;
import fr.afpa.bean.Utilisateur;

import fr.afpa.service.ProduitService;

/**
 * Servlet implementation class PanierServlet
 */
@WebServlet("/PanierServlet")
public class PanierServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PanierServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (request.getSession().getAttribute("user") != null) {
			Panier panier = (Panier) request.getSession().getAttribute("objetPanier");
			List<Produit> list = panier.getProduits();
			request.getSession().setAttribute("list", list);
			request.getRequestDispatcher("/jsp/Panier.jsp").forward(request, response);

		} else if (request.getSession().getAttribute("user") == null) {

			request.getRequestDispatcher("/ConnextionServlet").forward(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Panier panier = (Panier) request.getSession().getAttribute("objetPanier");

		String idString = request.getParameter("id");
		String quantiteString = request.getParameter("quantite");
		if (quantiteString.isEmpty()) {
			quantiteString = "1";
		}
		Integer quantite = Integer.parseInt(quantiteString.trim());
		Integer id = Integer.parseInt(idString.trim());
		Produit produit = ProduitService.getInstance().chercherUnProduit(id);
		produit.setQuantite(quantite);
		panier.getProduits().add(produit);
		request.getRequestDispatcher("/IndexServlet").forward(request, response);

	}

}
