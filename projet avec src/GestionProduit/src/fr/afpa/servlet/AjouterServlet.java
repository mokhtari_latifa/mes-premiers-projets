package fr.afpa.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.bean.Produit;
import fr.afpa.service.ProduitService;

/**
 * Servlet implementation class AjouterServlet
 */
@WebServlet("/AjouterServlet")
public class AjouterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AjouterServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		if(request.getSession().getAttribute("user")!=null) {
			
			request.getRequestDispatcher("/jsp/AjouterProduit.jsp").forward(request, response);
		}
		else if (request.getSession().getAttribute("user")==null) {
			
			request.getRequestDispatcher("/jsp/connextion.jsp").forward(request, response);
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String nom = request.getParameter("nom");
		String prix = request.getParameter("prix");
		String couleur = request.getParameter("couleur");
		String categorie = request.getParameter("categorie");
		String description = request.getParameter("description");

		Double prix2 = Double.parseDouble(prix);

		Produit produit = new Produit(nom, prix2, couleur,description, categorie);
		ProduitService.getInstance().addProduit(produit);
		response.sendRedirect(request.getContextPath() + "/IndexServlet");

	}

}
