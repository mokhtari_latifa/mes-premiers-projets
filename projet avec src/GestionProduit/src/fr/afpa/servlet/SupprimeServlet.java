package fr.afpa.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.service.ProduitService;

/**
 * Servlet implementation class SupprimeServlet
 */
@WebServlet("/SupprimeServlet")
public class SupprimeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SupprimeServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
	if(request.getSession().getAttribute("user")!=null) {
			String idrecup = request.getParameter("id");
			Integer id = Integer.parseInt(idrecup.trim());
			ProduitService.getInstance().supprimeProduit(id);
			request.getRequestDispatcher( "/IndexServlet").forward(request, response);
		} else if(request.getSession().getAttribute("user")==null) {
			
			request.getRequestDispatcher("/jsp/connextion.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
