package fr.afpa.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.Dao.UtilisateurDao;
import fr.afpa.bean.Utilisateur;
import fr.afpa.service.UtilisateurService;

/**
 * Servlet implementation class CreeCompte
 */
@WebServlet("/CreeCompteServlet")
public class CreeCompteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CreeCompteServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setAttribute("title", "Creer compte");
		this.getServletContext().getRequestDispatcher("/jsp/cree_user.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String login = request.getParameter("login");
		String mdp = request.getParameter("mdp");
	
		Utilisateur user = new Utilisateur( 1,nom, prenom, login, mdp);

		try {
			UtilisateurService.getInstance().saveUser(user);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		request.getRequestDispatcher( "/IndexServlet").forward(request, response);

	}

}
