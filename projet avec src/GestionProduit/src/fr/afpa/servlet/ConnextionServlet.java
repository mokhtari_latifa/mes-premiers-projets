package fr.afpa.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.bean.Panier;
import fr.afpa.bean.Utilisateur;
import fr.afpa.service.UtilisateurService;

/**
 * Servlet implementation class Connextion
 */
@WebServlet("/ConnextionServlet")
public class ConnextionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConnextionServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/jsp/connextion.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String login = request.getParameter("login");
		String passWord = request.getParameter("passWord");
		Utilisateur user =( Utilisateur) UtilisateurService.getInstance().seConnecter(login, passWord);
		if(user!=null) {
			Panier panier = new Panier();
			user.setPanier(panier);
			request.getSession().setAttribute("objetPanier", panier);
			request.getSession().setAttribute("user", user);
			request.getRequestDispatcher( "/IndexServlet").forward(request, response);
		} else if(user==null) {
			request.setAttribute("messageErreur", "Identifiants incorrects.");
			request.getRequestDispatcher( "/jsp/connextion.jsp").forward(request, response);
		}
		
	}

}
