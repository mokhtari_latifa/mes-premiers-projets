
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>

<title>Se connecter</title>
<link rel="stylesheet" href="css/connextion.css">
<link rel="stylesheet" href="css/ajouter.css">

<body>
	<jsp:include page="Header.jsp"></jsp:include>

	<h2>Se connecter</h2>

	<form action="<c:url value="/ConnextionServlet"/>" method="post">

		<div class="container">
			<input type="text" placeholder="Identifiant" name="login" required>
			<input type="password" placeholder="PassWord" name="passWord"
				required>

			<button type="submit">Valider</button>
			<label> <input type="checkbox" checked="checked"
				name="remember"> Rappel de moi
			</label>
		</div>

		<div class="container" style="background-color: #f1f1f1 ">
			<button type="button" class="cancelbtn">
				<a href="<c:url value="/IndexServlet"/>">Annuler</a>
			</button>
			<span class="psw">Cr�er un <a
				href="<c:url value="/CreeCompteServlet"/>">compte</a></span>
		</div>
	</form>
	<!--  le style j'ai le fait dans connextion.css mais marche pas du le fait de rajouter ici-->
	<div class="messageErreur" style="background-color: #f1f1f1; color: red ">
		<c:if test="${ not empty requestScope.messageErreur  }">
			
			<h2>${ requestScope.messageErreur} </h2>		
			</c:if>
	</div>

	<jsp:include page="Footer.jsp"></jsp:include>
</body>
</html>