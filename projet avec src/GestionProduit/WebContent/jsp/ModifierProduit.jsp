
<%@page import="fr.afpa.bean.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<meta charset="ISO-8859-1">
<title>Modifier Produit</title>
<link rel="stylesheet" href="css/ajouter.css">
<body>
	<jsp:include page="Header.jsp"></jsp:include>


	<section>


		<form action="<c:url value="/ModifierServlet"/>" method="POST"
			class="form-example">
			<fieldset>
				<legend>MODIFIER LE PRODUIT : </legend>
				<div class="form-example">
					<label for="id"> ID: </label> <input type="text" name="id"
						value="${ requestScope.produit.id} " readonly="readonly">
				</div>
				<div class="form-example">
					<label for="name"> Nom: </label> <input type="text" name="nom"
						value="${ requestScope.produit.nom} " required>
				</div>

				<div class="form-example">
					<label for="prix">Prix: </label> <input type="text" name="prix"
						value="${ requestScope.produit.prix} " required>
				</div>

				<div class="form-example">
					<label for="couleur">Couleur: </label> <input type="color"
						name="couleur" value="${ requestScope.produit.couleur}" required>
				</div>


				<br>

				<div class="form-example">
					<select name="categorie" selected>

						<option value="categorie1"
							${ requestScope.produit.categorie=='categorie1' ? 'slected':''}>Catégorie
							1</option>

						<option value="categorie2"
							${ requestScope.produit.categorie=='categorie2' ? 'slected':''}>Catégorie
							3</option>

						<option value="categorie3"
							${ requestScope.produit.categorie=='categorie3' ? 'slected':''}>Catégorie
							2</option>
						<option value="categorie4"
							${ requestScope.produit.categorie=='categorie4' ? 'slected':''}>Catégorie
							4</option>


				</div>
				<br>
				<div class="form-example">
					<textarea rows="4" cols="50" name="description"> ${requestScope.produit.description} </textarea>
				</div>
				<br>

				<div>
					<button class="button" type="submit">Modifier le produit</button>
				</div>
			</fieldset>
		</form>


	</section>
	<jsp:include page="Footer.jsp"></jsp:include>

</body>
</html>