
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<link rel="stylesheet" href="css/connextion.css">
<link rel="stylesheet" href="css/ajouter.css">
<jsp:include page="Header.jsp"></jsp:include>
<section id="creeCompte">
	<div class="container">
		<h1>Creer un compte</h1>
		<form action="<c:url value="/CreeCompteServlet"/>" method="post">

			<fieldset>

				<div>
					<label for="formPrenom"> Prenom</label> <input type="text"
						name="prenom" id="formPrenom">
				</div>
				<div>
					<label for="formNom"> Nom</label> <input type="text" name="nom"
						id="formNom">
				</div>

				<div>
					<label for="formEmail"> Identifiant</label> <input type="text"
						name="login" id="formEmail">
				</div>
				<div>
					<label for="formmdp"> Mot de passe</label> <input type="password"
						name="mdp" id="formmdp">
				</div>

				<div>
					<input type="submit" value="Cr�ation du compte">
					<button class="button" type="submit">
						<a href="<c:url value="IndexServlet"/>">Retour</a>
					</button>
				</div>
		</form>
	</div>

</section>
<jsp:include page="Footer.jsp"></jsp:include>