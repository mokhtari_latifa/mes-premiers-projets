
<%@page import="java.util.List"%>
<%@page import="fr.afpa.bean.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>

<title>Details Produit</title>
<link rel="stylesheet" href="css/ajouter.css">
<body>
	<jsp:include page="Header.jsp"></jsp:include>
	<div class="containt">

		<section class="detailProduit">

			<form action="<c:url value="/PanierServlet"/>" method="POST"
				class="form-example">

				<legend>DETAIL PRODUIT : </legend>
				<div class="form-example">
					<label for="id"> ID: </label> <input type="text" name="id"
						value="${ requestScope.produit.id} " readonly="readonly">
				</div>
				<div class="form-example">
					<label for="name"> Nom: </label> <input type="text" name="nom"
						value="${ requestScope.produit.nom} " readonly="readonly">
				</div>

				<div class="form-example">
					<label for="prix">Prix: </label> <input type="text" name="prix"
						value="${ requestScope.produit.prix} &#x20AC;" readonly="readonly"> 
				</div>

				<div class="form-example">
					<label for="couleur">Couleur: </label> <input type="color"
						name="couleur" value="${ requestScope.produit.couleur}"
						readonly="readonly">
				</div>


				<div class="form-example">
					<label for="prix">Cat�gorie: </label> <input type="text"
						name="prix" value="${ requestScope.produit.categorie} "
						readonly="readonly">

					<div class="form-example">
						<textarea rows="4" cols="50" name="description"
							readonly="readonly"> ${produit.description} </textarea>
					</div>
					<br>
					<div class="form-example">
						<label for="prix">Pour ajouter au Panier choisir Quantit�:
						</label> <input type="number" name="quantite" value="quantite" min="1" max="100" placeholder="1">
					</div>


					<div>
						<button class="button" type="submit">Ajouter Au Panier</button> <button class="button" type="submit"> <a href="<c:url value="IndexServlet"/>">Quitter</a></button>
					</div>
			</form>
		</section>
	</div>
	<jsp:include page="Footer.jsp"></jsp:include>
</body>
</html>