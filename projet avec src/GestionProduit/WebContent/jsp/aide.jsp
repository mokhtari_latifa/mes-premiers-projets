<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Aide</title>
<link rel="stylesheet" href="css/ajouter.css">
<link rel="stylesheet" href="css/header.css">
</head>
<body>
	<jsp:include page="Header.jsp"></jsp:include>
	<button class="button" type="submit"> <a href="<c:url value="IndexServlet"/>">Retour</a></button>

	<div class="divAide">

		<form METHOD=POST ACTION="mailto:elylatifa@hotmail.com">
			<ul>
				<li>
					<h1>Contact</h1>
					<p id="p1"></p>
				</li>
				<li><label>Civilit� </label> <input type="radio"
					name="civilite" value="1" required> M. <input type="radio"
					name="civilite" value="2" required> Mme <input type="radio"
					name="civilite" value="3" required> Melle</li>
				<li><label for="Nom">Nom </label> <input type="text" name="Nom"
					id="Nom" pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}$" required></li>
				<li><label for="Prenom">Pr&eacute;nom </label> <input
					type="text" name="Prenom" id="Prenom"
					pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}$" required></li>
				<li><label for="Email">Email </label> <input type="email"
					name="Email" id="Email"
					pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required>
				</li>
				<li><label for="Choix">Sujet </label> <select name="Choix"
					id="Choix" required>
						<option value="0">S&eacute;lectionnner</option>
						<option value="1">Apr&egrave;s-vente, &eacute;change et
							retour</option>
						<option value="2">Probl&egrave;me technique</option>
						<option value="3">Autres sujets</option>
				</select></li>
				<li>
					<div id="idTextarea">
						<p>Message</p>

						<textarea name="Message" id="Message" cols="30" rows="4"></textarea>
					</div>
				</li>
				<li><input type="submit" value="Envoyer" id="submit"></li>
			</ul>
		</form>
	</div>


	<jsp:include page="Footer.jsp"></jsp:include>
</body>
</html>