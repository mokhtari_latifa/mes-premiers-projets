
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<link rel="stylesheet" href="css/ajouter.css">
<title>Ajouter UN produit</title>
<body>
	<jsp:include page="Header.jsp"></jsp:include>
	<div class="containt">

		<form action="<c:url value="/AjouterServlet"/>"
			method="POST" class="form-example">
			<fieldset>
				<legend>AJOUTER UN PRODUIT</legend>
				<div class="form-example">
					<label for="name"> Nom: </label> <input type="text" name="nom"
						required>
				</div>

				<div class="form-example">
					<label for="prix">Prix: </label> <input type="number" name="prix"
						required><span>&#x20AC;</span>
				</div>

				<div class="form-example">
					<label for="couleur">Couleur: </label> <input type="color"
						name="couleur" required>
				</div>

				<div class="form-example">
					<select name="categorie">
						<option value="categorie1">Cat�gorie1</option>
						<option value="categorie2">Cat�gorie2</option>
						<option value="categorie3">Cat�gorie3</option>
						<option value="categorie4">Cat�gorie4</option>
					</select>
				</div>
				<br>
				<div class="form-example">
					<textarea rows="4" cols="50" name="description">
            Description...</textarea>
				</div>
				<br>

				<div >
					<button  class="button" type="submit">Cr�er un produit</button> <button class="button" type="submit"> <a href="<c:url value="IndexServlet"/>">Retour</a></button>
				</div>
			</fieldset>
		</form>

	</div>
	<jsp:include page="Footer.jsp"></jsp:include>
</body>
</html>