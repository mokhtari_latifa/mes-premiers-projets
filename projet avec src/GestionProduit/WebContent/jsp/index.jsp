<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.util.List"%>
<%@page import="fr.afpa.bean.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>HOME</title>
<link href="https://fonts.googleapis.com/css?family=Do+Hyeon"
	rel="stylesheet">

<link rel="stylesheet" href="css/ajouter.css">
</head>
<body>
	<jsp:include page="Header.jsp"></jsp:include>
	<div class="containt">
		<h1>Liste des produits</h1>

		<table id="customers">
			<thead class="grey lighten-2">
				<tr>
					<th scope="col">ID</th>
					<th scope="col">Nom</th>

				</tr>
			</thead>
			<tbody>

				<c:if test="${empty requestScope.produits  }">
			
			"Pas de r�sultat" 
			
			</c:if>
				<c:if test="${not empty requestScope.produits }">
					<c:forEach var="produit" items="${requestScope.produits}">
						<tr>
							<th scope="row">${produit.id }</th>
							<td><a href="DetailServlet?id=${produit.id }">${produit.nom}</a></td>
							<td><a href="ModifierServlet?id=${produit.id }"><img
									class="tools" src="img/modifier.jpg" alt="Modifier"></a></td>
							<td><a href="SupprimeServlet?id=${produit.id }"><img
									class="tools" src="img/supprime.png" alt="Supprimer"></a></td>
						</tr>

					</c:forEach>

				</c:if>
			</tbody>
		</table>
	<br>
		<button  class="button">
			<a href="<%=request.getContextPath()%>/AjouterServlet"> Ajouter
				un produit</a>
		</button>
	</div>

	<jsp:include page="Footer.jsp"></jsp:include>
</body>
</html>