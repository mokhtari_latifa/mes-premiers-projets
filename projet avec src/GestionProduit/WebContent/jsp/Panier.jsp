
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<title>Panier</title>
<link rel="stylesheet" href="css/ajouter.css">

<body>
	<jsp:include page="Header.jsp"></jsp:include>

	<h1>Voter panier</h1>
	<fieldset id="mycart" class="cart">
		<legend>Voter panier</legend>
		<div id="cartArea">
			<c:if test="${empty sessionScope.list  }">
			
			"Le panier ne contient pas de produit " 
			
			</c:if>
			<c:if test="${not empty sessionScope.list}">
				<c:forEach var="produit" items="${sessionScope.list}">
					<ul>
						<span> Produit ID</span>
						<li>${ produit.id }</li>
						<span> Nom:</span>
						<li>${produit.nom}</li>
						<span> Prix:</span>
						<li>${produit.prix }&#x20AC;</li>
						<span> Description:</span>
						<li>${produit.description}</li>
						<span> Quantite:</span>
						<li>${produit.quantite}</li>
						<li id="panier"><span> <i
								class="fas fa-shopping-basket"></i>
						</span> <a
							href="<c:url value="SupprimerProduitDePanier?id=${produit.id} "/>">Supprime</a>
						</li>

					</ul>

				</c:forEach>

			</c:if>
		</div>
	</fieldset>
	<button class="button" type="submit">
		<a href="<c:url value="IndexServlet"/>">Retour</a>
	</button>

	<jsp:include page="Footer.jsp"></jsp:include>

</body>
</html>