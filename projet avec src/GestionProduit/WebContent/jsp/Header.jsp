
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<link rel="stylesheet" href="css/header.css">

<header>
	 <span id="miniMenu">
		<ul>
			<li><a href="<c:url value="AideServlet"/>">Aide</a></li>

			<li id="panier"><span> <i class="fas fa-shopping-basket"></i>
			</span> <a href="<c:url value="PanierServlet"/>">Panier</a></li>

	</span>
		<li><i class="fas fa-user"></i> <a
			href="<c:url value="IndexServlet"/>">Accueil</a></li>
		<c:if test="${empty sessionScope.user  }">
			<li><a href="<c:url value="/ConnextionServlet"/>">Se
					connecter </a></li>
		</c:if>
		<c:if test="${not empty sessionScope.user  }">
			<li><a href="<c:url value="/DeconnexionServlet"/>">Déconnexion
			</a></li>
			<li>Bonjour ${sessionScope.user.nom } ${sessionScope.user.prenom }</li>
		</c:if>
	</ul>

</header>