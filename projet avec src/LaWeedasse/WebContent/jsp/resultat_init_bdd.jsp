<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<c:if test="${ok}" var="resultatTest">
	<h1 style="color: blue">initialisation OK</h1>
	<a href="accueil.do"> vers l'accueil </a>
</c:if>
<c:if test="${not resultatTest}">
	<h1 style="color: red">initialisation KO</h1>
</c:if>
</body>
</html>