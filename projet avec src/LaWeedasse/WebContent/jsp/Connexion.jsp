<%@page import="org.apache.struts.action.ActionErrors"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<link crossorigin="anonymous"
	href="https://maxcdn.bootstrapcdn.com/
bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	rel="stylesheet">
<link crossorigin="anonymous"
	href="https://maxcdn.bootstrapcdn.com/
bootstrap/3.3.7/css/bootstrap-theme.min.css"
	integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
	rel="stylesheet">

<script crossorigin="anonymous"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js">
	
</script>

<title>La Weedasse</title>
</head>
<body>
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="col-lg-12">
				<img alt="Banniere" src="/static/img/Banweed.png" />
			</div>
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/LaWeedasse/Accueil">LOGO</a>
			</div>

		
			<!--/.nav-collapse -->
		</div>
	</nav>

	<div class="container">

		<div class="starter-template">
			<BR>
			<BR> <BR> <BR>
			<BR> <BR>
			
				<html:form method="post"
					action="/connexion.do">
					<div>
						<html:errors property="<%=ActionErrors.GLOBAL_MESSAGE%>" />
					</div>
					<p>

						<label for="nom">Nom:</label>
						<html:text property="email" />
						<label for="mdp">mdp:</label>
						<html:password property="password" />

						<c:if test="${ not empty messageErreur }">
							<div style="color: red">${ messageErreur }</div>
						</c:if>
					</p>
					<html:submit value="Connexion" />
				</html:form>
			
</body>
</html>