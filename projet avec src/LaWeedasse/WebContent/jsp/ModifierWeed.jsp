<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="fr.afpa.dto.WeedDto"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<link crossorigin="anonymous"
	href="https://maxcdn.bootstrapcdn.com/
bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	rel="stylesheet">
<link crossorigin="anonymous"
	href="https://maxcdn.bootstrapcdn.com/
bootstrap/3.3.7/css/bootstrap-theme.min.css"
	integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
	rel="stylesheet">

<script crossorigin="anonymous"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js">
	
</script>

<title>La Weedasse</title>
</head>
<body>

	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="col-lg-12">
				<img alt="Banniere" src="/static/img/Banweed.png" />
			</div>
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/LaWeedasse/Accueil">LOGO</a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="accueil.do">Accueil</a></li>
					<li><a href="boutique.do">Boutique</a></li>
					<li><a href="contact.do">Contact</a></li>
					<li><a href="profil.do">Profil</a></li>

				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>

	<div class="container">

		<div class="starter-template">
			<BR> <BR> <BR> <BR> <BR> <BR> >
			<h2>Rentrez les nouvelles informations de l'article</h2>
			<html:form method="POST" action="/modifier.do">
				<html:text property="id" value="${requestScope.weed.id}" />
				<br>
				<label for="id">id :</label>
				<html:text property="id" 
				value="${requestScope.weed.id}" /> 
				<br>
				<label for="nom">nom :</label>
				<html:text property="nom" 
					value="${requestScope.weed.nom}" />
				<br>
				<label for="prix">prix :</label>
				<html:text property="prix" 
					value="${requestScope.weed.prix}" />
				<br>
				<label for="poid">poid :</label>
				<html:text property="poid" 
					value="${requestScope.weed.poid}" />
				<br>

				<html:submit value="Modifier l'article" />
			</html:form>
</body>
</html>