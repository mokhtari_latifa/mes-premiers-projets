package fr.afpa.presentation.actionForm;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class ModifierForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3394551871835109141L;

	private String id;
	private String nom;
	private String prix;
	private String poid;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the prix
	 */
	public String getPrix() {
		return prix;
	}

	/**
	 * @param prix the prix to set
	 */
	public void setPrix(String prix) {
		this.prix = prix;
	}

	/**
	 * @return the poid
	 */
	public String getPoid() {
		return poid;
	}

	/**
	 * @param poid the poid to set
	 */
	public void setPoid(String poid) {
		this.poid = poid;
	}

	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);
	}

	@Override
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
		final ActionErrors errors = new ActionErrors();
		// Id obligatoire
		if ("".equals(id)) {
			errors.add(ActionErrors.GLOBAL_MESSAGE, new ActionMessage("error.modifier.id.obligatoire"));
		}

		if (!isInteger(id)) {
			errors.add(ActionErrors.GLOBAL_MESSAGE, new ActionMessage("error.modifier.id.format"));
		}

		// Nom obligatoire
		if (nom.isEmpty()) {
			errors.add(ActionErrors.GLOBAL_MESSAGE, new ActionMessage("error.modifier.nom.obligatoire"));
		}
		if ("".equals(prix)) {
			errors.add(ActionErrors.GLOBAL_MESSAGE, new ActionMessage("error.modifier.prix.obligatoire"));
		}

		if (!isInteger(prix)) {
			errors.add(ActionErrors.GLOBAL_MESSAGE, new ActionMessage("error.modifier.prix.format"));
		}
		if ("".equals(poid)) {
			errors.add(ActionErrors.GLOBAL_MESSAGE, new ActionMessage("error.modifier.poid.obligatoire"));
		}

		if (!isFloat(poid)) {
			errors.add(ActionErrors.GLOBAL_MESSAGE, new ActionMessage("error.modifier.poid.format"));
		}

		return errors;
	}

	private boolean isInteger(String val) {
		try {
			Integer.parseInt(val);
			return true;
		} catch (final NumberFormatException e) {

		}

		return false;
	}

	private boolean isFloat(String val) {
		try {
			Float.parseFloat(val);
			return true;
		} catch (final NumberFormatException e) {

		}

		return false;
	}
}
