package fr.afpa.presentation.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import fr.afpa.service.ServiceFactory;
import fr.afpa.service.interfaces.IWeedService;

public class SupprimerAction extends Action{
	private IWeedService  weedService = new ServiceFactory().getWeedService(ServiceFactory.TYPE_WEED_SERVICE_BDD);

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String idString = request.getParameter("id");

		Integer id = Integer.parseInt(idString);

		weedService.deleteWeed(id);

		return mapping.findForward("success");
	}
	

}
