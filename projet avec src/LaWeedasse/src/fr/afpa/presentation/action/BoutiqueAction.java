package fr.afpa.presentation.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import fr.afpa.dto.WeedDto;
import fr.afpa.service.ServiceFactory;
import fr.afpa.service.interfaces.IWeedService;

public class BoutiqueAction extends Action {
	private IWeedService weedService = new ServiceFactory().getWeedService(ServiceFactory.TYPE_WEED_SERVICE_BDD);
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
List<WeedDto> weeds = weedService.getAllWeed();
		
		request.setAttribute("weeds", weeds);
		return mapping.findForward("success");
	}
	

}
