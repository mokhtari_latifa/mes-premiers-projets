package fr.afpa.presentation.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import fr.afpa.dto.WeedDto;
import fr.afpa.presentation.actionForm.ModifierForm;
import fr.afpa.service.ServiceFactory;
import fr.afpa.service.interfaces.IWeedService;

public class ModifierAction extends Action {
	private IWeedService weedService = new ServiceFactory().getWeedService(ServiceFactory.TYPE_WEED_SERVICE_BDD);

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ModifierForm modform = (ModifierForm)form;
	
			WeedDto w = new WeedDto(Integer.parseInt(modform.getId()), modform.getNom(),
					Integer.parseInt(modform.getPrix()), Float.parseFloat(modform.getPoid()));
			boolean modresul = weedService.updateArticle(w);
			if(modresul) {
				return mapping.findForward("success");
				
			}
			return mapping.findForward("echec");
			
	}

}
