package fr.afpa.presentation.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import fr.afpa.dto.WeedDto;
import fr.afpa.presentation.actionForm.AjouterForm;
import fr.afpa.service.ServiceFactory;
import fr.afpa.service.interfaces.IWeedService;

public class AjouterAction extends Action {
	private IWeedService weedService = new ServiceFactory().getWeedService(ServiceFactory.TYPE_WEED_SERVICE_BDD);

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		AjouterForm ajouterForm = (AjouterForm)form;
		try {
			WeedDto w = new WeedDto(Integer.parseInt(ajouterForm.getId()), ajouterForm.getNom(),
					Integer.parseInt(ajouterForm.getPrix()), Float.parseFloat(ajouterForm.getPoid()));
			weedService.addWeed(w);
			return mapping.findForward("success");
		} catch (NumberFormatException e) {

			e.printStackTrace();
			return mapping.findForward("echec");
		}

	}

}
