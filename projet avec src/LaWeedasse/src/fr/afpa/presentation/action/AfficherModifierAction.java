package fr.afpa.presentation.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import fr.afpa.dto.WeedDto;
import fr.afpa.service.ServiceFactory;
import fr.afpa.service.interfaces.IWeedService;

public class AfficherModifierAction extends Action{
	private IWeedService weedService=  new ServiceFactory().getWeedService(ServiceFactory.TYPE_WEED_SERVICE_BDD);

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		WeedDto weed = weedService.chercherUneWeed(Integer.parseInt(request.getParameter("id")));
		request.setAttribute("weed", weed);
		return mapping.findForward("success");
	}
	

}
