package fr.afpa.dto;

import java.util.ArrayList;
import java.util.List;

public class UserDto {
	private int id;
	private String nom;
	private String mdp;
	private String prenom;
	private String adresse;
	private Integer tel;
	private String email;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	private List<WeedDto> panierWeeds = new ArrayList<WeedDto>();

	public UserDto() {
		super();
	}

	public UserDto(int id, String nom, String mdp, String prenom, String adresse, Integer tel, String email) {
		super();
		this.id = id;
		this.nom = nom;
		this.mdp = mdp;
		this.prenom = prenom;
		this.adresse = adresse;
		this.tel = tel;
		this.email = email;
		this.panierWeeds = null;
	}

	public String getMdp() {
		return mdp;
	}

	public void setMdp(String mdp) {
		this.mdp = mdp;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public int getTel() {
		return tel;
	}

	public void setTel(int tel) {
		this.tel = tel;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<WeedDto> getPanierWeeds() {
		return panierWeeds;
	}

	public void setPanierWeeds(List<WeedDto> panierWeeds) {
		this.panierWeeds = panierWeeds;
	}

	public void ajouterPanier(WeedDto weed) {
		panierWeeds.add(weed);
	}

	public void setTel(Integer tel) {
		this.tel = tel;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", nom=" + nom + ", mdp=" + mdp + ", prenom=" + prenom + ", adresse=" + adresse
				+ ", tel=" + tel + ", email=" + email + ", panierWeeds=" + panierWeeds + "]";
	}

}
