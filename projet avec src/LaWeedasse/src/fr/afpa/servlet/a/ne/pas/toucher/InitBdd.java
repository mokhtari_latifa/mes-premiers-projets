package fr.afpa.servlet.a.ne.pas.toucher;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.dao.ConnectionPostgreSQL;

/**
 * Servlet implementation class InitBdd
 */
public class InitBdd extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public InitBdd() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("/jsp/initBdd.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String host = request.getParameter("host");
		String port = request.getParameter("port");
		String bdd = request.getParameter("bdd");
		String user = request.getParameter("user");
		String passwd = request.getParameter("passwd");

		ConnectionPostgreSQL.initConnexion(host, port, bdd, user, passwd);

		boolean resultat = ConnectionPostgreSQL.initBdd();
		request.setAttribute("ok", resultat);
		request.getRequestDispatcher("/jsp/resultat_init_bdd.jsp").forward(request, response);
	}

}
