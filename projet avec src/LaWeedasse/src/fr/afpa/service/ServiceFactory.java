package fr.afpa.service;

import fr.afpa.service.interfaces.IUserService;
import fr.afpa.service.interfaces.IWeedService;

public class ServiceFactory {

	public static final int TYPE_WEED_SERVICE_BDD = 1;
	public static final int TYPE_WEED_SERVICE_REST = 2;

	public static final int TYPE_USER_SERVICE_BDD = 1;
	public static final int TYPE_USER_SERVICE_REST = 2;
	public static final int TYPE_USER_SERVICE_LDAP = 3;

	public IUserService getUserService(int typeProduit) {
		IUserService produitA = null;

		switch (typeProduit) {
		case TYPE_USER_SERVICE_BDD:
			produitA = new UserServiceBDD();
			break;

		default:
			throw new IllegalArgumentException("Type de produit inconnu");
		}

		return produitA;
	}

	public IWeedService getWeedService(int typeProduit) {
		IWeedService produitA = null;

		switch (typeProduit) {
		case TYPE_USER_SERVICE_BDD:
			produitA = new WeedServiceBDD();
			break;

		default:
			throw new IllegalArgumentException("Type de produit inconnu");
		}

		return produitA;
	}
}
