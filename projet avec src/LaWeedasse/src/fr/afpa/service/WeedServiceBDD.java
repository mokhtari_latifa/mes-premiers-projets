package fr.afpa.service;

import java.util.List;

import fr.afpa.dao.DaoFactory;
import fr.afpa.dao.interfaces.IWeedDao;
import fr.afpa.dto.WeedDto;
import fr.afpa.entities.Weed;
import fr.afpa.service.interfaces.IWeedService;
import fr.afpa.service.mappers.WeedMapper;

public class WeedServiceBDD implements IWeedService {

	private IWeedDao weedDao;
	private WeedMapper weedMapper = new WeedMapper();

	public WeedServiceBDD() {
		super();
		DaoFactory daoFactory = new DaoFactory();
		weedDao = daoFactory.getWeedDao(DaoFactory.TYPE_WEED_DAO_PSQL);
	}

	public WeedServiceBDD(IWeedDao weedDao) {
		super();
		this.weedDao = weedDao;
	}

	@Override
	public List<WeedDto> getAllWeed() {
		List<Weed> weed = weedDao.getAllWeed();
		return weedMapper.mapToDtos(weed);
	}

	@Override
	public WeedDto chercherUneWeed(Integer id) {

		Weed weed = weedDao.chercherUneWeed(id);
		return weedMapper.mapToDto(weed);
	}

	@Override
	public void addWeed(WeedDto weedDto) {
		Weed weed = weedMapper.mapToDo(weedDto);

		weedDao.addWeed(weed);
	}

	@Override
	public void deleteWeed(WeedDto weedDto) {
		Weed weed = weedMapper.mapToDo(weedDto);
		deleteWeed(weed.getId());
	}

	@Override
	public void deleteWeed(Integer id) {
		weedDao.deleteWeed(id);

	}

	@Override
	public boolean updateArticle(WeedDto weedDto) {
		Weed weed = weedMapper.mapToDo(weedDto);
		return weedDao.updateArticle(weed);
	}
}
