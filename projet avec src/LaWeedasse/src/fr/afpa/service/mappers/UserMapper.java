package fr.afpa.service.mappers;

import java.util.ArrayList;
import java.util.List;

import fr.afpa.dto.UserDto;
import fr.afpa.entities.User;

public class UserMapper {

	public UserDto mapToDto(User user) {

		if(user==null)
				return null;
		UserDto userDto = new UserDto();

		userDto.setId(user.getId());
		userDto.setEmail(user.getEmail());
		userDto.setMdp(user.getMdp());
		userDto.setNom(user.getNom());
		userDto.setPrenom(user.getPrenom());
		userDto.setTel(user.getTel());

		return userDto;
	}

	public List<UserDto> mapToDtos(List<User> users) {
		if(users==null)
			return null;
		List<UserDto> resultat = new ArrayList<UserDto>();

		for (User user : users) {
			resultat.add(mapToDto(user));
		}
		return resultat;
	}

	public User mapToDo(UserDto userDto) {
		if(userDto==null)
			return null;
		User user = new User();

		user.setId(userDto.getId());
		user.setEmail(userDto.getEmail());
		user.setMdp(userDto.getMdp());
		user.setNom(userDto.getNom());
		user.setPrenom(userDto.getPrenom());
		user.setTel(userDto.getTel());

		return user;
	}

	public List<User> mapToDos(List<UserDto> usersDto) {
		if(usersDto==null)
			return null;
		List<User> resultat = new ArrayList<User>();

		for (UserDto userDto : usersDto) {
			resultat.add(mapToDo(userDto));
		}
		return resultat;
	}

}
