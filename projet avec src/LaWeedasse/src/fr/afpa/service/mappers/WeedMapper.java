package fr.afpa.service.mappers;

import java.util.ArrayList;
import java.util.List;

import fr.afpa.dto.WeedDto;
import fr.afpa.entities.Weed;

public class WeedMapper {

	public WeedDto mapToDto(Weed weed) {

		WeedDto weedDto = new WeedDto();

		weedDto.setId(weed.getId());
		weedDto.setNom(weed.getNom());
		weedDto.setPoid(weed.getPoid());
		weedDto.setPrix(weed.getPrix());

		return weedDto;
	}

	public List<WeedDto> mapToDtos(List<Weed> weeds) {

		List<WeedDto> resultat = new ArrayList<WeedDto>();

		for (Weed user : weeds) {
			resultat.add(mapToDto(user));
		}
		return resultat;
	}

	public Weed mapToDo(WeedDto weedDto) {

		Weed weed = new Weed();

		weed.setId(weedDto.getId());
		weed.setNom(weedDto.getNom());
		weed.setPoid(weedDto.getPoid());
		weed.setPrix(weedDto.getPrix());

		return weed;
	}

	public List<Weed> mapToDos(List<WeedDto> weedDtos) {

		List<Weed> resultat = new ArrayList<Weed>();

		for (WeedDto userDto : weedDtos) {
			resultat.add(mapToDo(userDto));
		}
		return resultat;
	}

}
