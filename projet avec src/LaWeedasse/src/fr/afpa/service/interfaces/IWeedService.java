package fr.afpa.service.interfaces;

import java.util.List;

import fr.afpa.dto.WeedDto;

public interface IWeedService {

	public List<WeedDto> getAllWeed();

	public WeedDto chercherUneWeed(Integer id);

	public void addWeed(WeedDto weed);

	public void deleteWeed(WeedDto weed);

	public void deleteWeed(Integer id);

	public boolean updateArticle(WeedDto weed);
}
