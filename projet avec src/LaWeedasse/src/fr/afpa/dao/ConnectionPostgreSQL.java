package fr.afpa.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class ConnectionPostgreSQL {

	/**
	 * URL de connection
	 */
	private static String url;

	private static String host = "localhost";

	private static String port = "5432";

	private static String bdd = "bdd_weed_jovany";
	/**
	 * Nom du user
	 */
	private static String user = "cda_user";
	/**
	 * Mot de passe du user
	 */
	private static String passwd = "cda";

	/**
	 * Objet de configuration
	 */
	private static Properties props = new Properties();

	/**
	 * Objet Connection
	 */
	private static Connection connect;

	/**
	 * M�thode qui va nous retourner notre instance et la cr�er si elle n'existe
	 * pas...
	 * 
	 * @return connect
	 */

	public static void initConnexion(String _host, String _port, String _bdd, String _user, String _passwd) {

		host = _host;
		port = _port;
		bdd = _bdd;
		user = _user;
		passwd = _passwd;

	}

	public static Connection getInstance() {
		if (ConnectionPostgreSQL.connect == null) {
			initConnection();
		}
		return ConnectionPostgreSQL.connect;
	}

	private static void initConnection() {
		url = "jdbc:postgresql://" + host + ":" + port + "/" + bdd;
		try {
			Class.forName("org.postgresql.Driver");
			props.setProperty("user", user);
			props.setProperty("password", passwd);
			ConnectionPostgreSQL.connect = DriverManager.getConnection(url, props);
			System.out.println("connexion ok!");
		} catch (SQLException e) {
			System.out.println("Impossible de se connecter � l�url : " + url);
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static boolean initBdd() {
		initConnection();
		Connection connexion = ConnectionPostgreSQL.connect;
		Statement statement = null;
		ResultSet resultat = null;
		try {

			statement = connexion.createStatement();
			statement.addBatch("drop  table IF EXISTS users;");
			statement.addBatch("drop  table IF EXISTS weeds");
			statement.addBatch("CREATE TABLE users(\r\n" + "    id serial PRIMARY KEY NOT NULL,\r\n"
					+ "    prenom VARCHAR,\r\n" + "    mdp VARCHAR,\r\n" + "    nom VARCHAR,\r\n"
					+ "    adresse VARCHAR,\r\n" + "    tel numeric,\r\n" + "    email VARCHAR\r\n" + ");");
			statement.addBatch("CREATE TABLE weeds(\r\n" + "    id serial PRIMARY KEY NOT NULL,\r\n"
					+ "    nom VARCHAR,\r\n" + "    prix numeric,\r\n" + "    poid numeric(6,2)\r\n" + ");");
			statement.addBatch("INSERT INTO users (id,prenom,mdp,nom,adresse,tel,email)\r\n"
					+ "VALUES   (1,'Tabouley',md5('azerty'),'Lemoine','47 rue kepler',41,'Lemoine@gmail.com'),\r\n"
					+ "        (2,'Eloise',md5('qsdfghs'),'Carton','47 rue kepler',87,'Eloise@gmail.com'),\r\n"
					+ "        (3,'Linux',md5('wxcvbn'),'Babine','47 rue kepler',60,'Linux@gmail.com');");
			statement.addBatch("INSERT INTO weeds (id,nom,prix,poid)\r\n" + "VALUES (1,'Cookie Kush',10,1.0),\r\n"
					+ "(2,'Orange Bud',18,0.7),\r\n" + "(3,'Lemon Skunk',12,1.1);");

			int[] nbrLigne = statement.executeBatch();

			System.out.println("nombre de lignes supprim�es : " + nbrLigne);
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (resultat != null) {
					resultat.close();
				}
				if (statement != null) {
					statement.close();
				}

			} catch (SQLException ignore) {

			}
		}

		return false;
	}

	/*
	
	
	
	*/

}