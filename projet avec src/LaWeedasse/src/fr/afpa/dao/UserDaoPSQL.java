package fr.afpa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.afpa.dao.interfaces.IUserDao;
import fr.afpa.entities.User;

public class UserDaoPSQL implements IUserDao {
	@Override
	public List<User> getAllUsers() {

		List<User> users = new ArrayList<User>();
		Connection connexion = ConnectionPostgreSQL.getInstance();
		Statement statement = null;
		ResultSet resultat = null;
		try {

			statement = connexion.createStatement();

			resultat = statement.executeQuery("SELECT id,prenom,mdp,nom,adresse,tel,email FROM users");

			while (resultat.next()) {
				Integer idUser = resultat.getInt("id");
				String prenom = resultat.getString("prenom");
				String mdp = resultat.getString("mdp");
				String nom = resultat.getString("nom");
				String adresse = resultat.getString("adresse");
				Integer tel = resultat.getInt("tel");
				String email = resultat.getString("email");
				User user = new User(idUser, nom, mdp, prenom, adresse, tel, email);

				users.add(user);

			}
		} catch (SQLException e) {

		} finally {
			try {
				if (resultat != null) {
					resultat.close();
				}
				if (statement != null) {
					statement.close();
				}

			} catch (SQLException ignore) {

			}
		}
		return users;
	}

	@Override
	public User chercherUnUser(Integer id) {

		Connection connexion = ConnectionPostgreSQL.getInstance();
		PreparedStatement statement = null;
		ResultSet resultat = null;
		try {

			statement = connexion
					.prepareStatement("SELECT id,prenom,mdp,nom,adresse,tel,email FROM users where id = ?");
			statement.setInt(1, id);

			resultat = statement.executeQuery();

			while (resultat.next()) {
				Integer idUser = resultat.getInt("id");
				String prenom = resultat.getString("prenom");
				String mdp = resultat.getString("mdp");
				String nom = resultat.getString("nom");
				String adresse = resultat.getString("adresse");
				Integer tel = resultat.getInt("tel");
				String email = resultat.getString("email");
				User user = new User(idUser, nom, mdp, prenom, adresse, tel, email);

				return user;

			}
		} catch (SQLException e) {

		} finally {
			try {
				if (resultat != null) {
					resultat.close();
				}
				if (statement != null) {
					statement.close();
				}

			} catch (SQLException ignore) {

			}
		}
		return null;
	}

	@Override
	public User chercherUnUser(String prenomP) {

		Connection connexion = ConnectionPostgreSQL.getInstance();
		PreparedStatement statement = null;
		ResultSet resultat = null;
		try {

			statement = connexion
					.prepareStatement("SELECT id,prenom,mdp,nom,adresse,tel,email FROM users where prenom = ?");
			statement.setString(1, prenomP);

			resultat = statement.executeQuery();

			while (resultat.next()) {
				Integer idUser = resultat.getInt("id");
				String prenom = resultat.getString("prenom");
				String mdp = resultat.getString("mdp");
				String nom = resultat.getString("nom");
				String adresse = resultat.getString("adresse");
				Integer tel = resultat.getInt("tel");
				String email = resultat.getString("email");
				User user = new User(idUser, nom, mdp, prenom, adresse, tel, email);

				return user;

			}
		} catch (SQLException e) {

		} finally {
			try {
				if (resultat != null) {
					resultat.close();
				}
				if (statement != null) {
					statement.close();
				}

			} catch (SQLException ignore) {

			}
		}
		return null;
	}

	@Override
	public void deleteUser(Integer id) {
		Connection connexion = ConnectionPostgreSQL.getInstance();
		PreparedStatement statement = null;
		ResultSet resultat = null;
		try {

			statement = connexion.prepareStatement("delete FROM users where id = ?");
			statement.setInt(1, id);

			int nbrLigne = statement.executeUpdate();
			System.out.println("nombre de lignes supprimées : " + nbrLigne);
		} catch (SQLException e) {

		} finally {
			try {
				if (resultat != null) {
					resultat.close();
				}
				if (statement != null) {
					statement.close();
				}

			} catch (SQLException ignore) {

			}
		}
	}

	@Override
	public void save(User user) {
		Connection connexion = ConnectionPostgreSQL.getInstance();
		PreparedStatement statement = null;
		ResultSet resultat = null;
		try {

			statement = connexion
					.prepareStatement("INSERT INTO USERS (prenom,mdp,nom,adresse,tel,email) VALUES(?,?,?,?,?,?)");
			statement.setString(1, user.getPrenom());
			statement.setString(2, user.getMdp());
			statement.setString(3, user.getNom());
			statement.setString(4, user.getAdresse());
			statement.setInt(5, user.getTel());
			statement.setString(6, user.getEmail());

			int nbrLigne = statement.executeUpdate();
			System.out.println("nombre de lignes supprimées : " + nbrLigne);
		} catch (SQLException e) {

		} finally {
			try {
				if (resultat != null) {
					resultat.close();
				}
				if (statement != null) {
					statement.close();
				}

			} catch (SQLException ignore) {

			}
		}
	}

	@Override
	public void update(User user) {
		Connection connexion = ConnectionPostgreSQL.getInstance();
		PreparedStatement statement = null;
		ResultSet resultat = null;
		try {

			statement = connexion
					.prepareStatement("UPDATE USERS SET  prenom=?,mdp=?,nom=?,adresse=?,tel=?, email=? WHERE id=?");
			statement.setString(1, user.getPrenom());
			statement.setString(2, user.getMdp());
			statement.setString(3, user.getNom());
			statement.setString(4, user.getAdresse());
			statement.setInt(5, user.getTel());
			statement.setString(6, user.getEmail());
			statement.setInt(7, user.getId());

			int nbrLigne = statement.executeUpdate();
			System.out.println("nombre de lignes supprimées : " + nbrLigne);
		} catch (SQLException e) {

		} finally {
			try {
				if (resultat != null) {
					resultat.close();
				}
				if (statement != null) {
					statement.close();
				}

			} catch (SQLException ignore) {

			}
		}
	}

	@Override
	public User chercherUserParLoginPassword(String login, String password) {

		Connection connexion = ConnectionPostgreSQL.getInstance();
		PreparedStatement statement = null;
		ResultSet resultat = null;
		try {

			statement = connexion.prepareStatement(
					"SELECT id,prenom,mdp,nom,adresse,tel,email FROM users where lower(email) = lower(?) and mdp=md5(?)");
			statement.setString(1, login);
			statement.setString(2, password);
			resultat = statement.executeQuery();

			while (resultat.next()) {
				Integer idUser = resultat.getInt("id");
				String prenom = resultat.getString("prenom");
				String mdp = resultat.getString("mdp");
				String nom = resultat.getString("nom");
				String adresse = resultat.getString("adresse");
				Integer tel = resultat.getInt("tel");
				String email = resultat.getString("email");
				User user = new User(idUser, nom, mdp, prenom, adresse, tel, email);

				return user;

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (resultat != null) {
					resultat.close();
				}
				if (statement != null) {
					statement.close();
				}

			} catch (SQLException ignore) {

			}
		}
		return null;
	}

}
