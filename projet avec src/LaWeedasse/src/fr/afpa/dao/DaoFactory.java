package fr.afpa.dao;

import fr.afpa.dao.interfaces.IUserDao;
import fr.afpa.dao.interfaces.IWeedDao;

public class DaoFactory {

	public static final int TYPE_WEED_DAO_PSQL = 1;

	public static final int TYPE_USER_DAO_PSQL = 1;

	public IUserDao getUserDao(int typeProduit) {
		IUserDao produitA = null;

		switch (typeProduit) {
		case TYPE_USER_DAO_PSQL:
			produitA = new UserDaoPSQL();
			break;

		default:
			throw new IllegalArgumentException("Type de produit inconnu");
		}

		return produitA;
	}

	public IWeedDao getWeedDao(int typeProduit) {
		IWeedDao produitA = null;

		switch (typeProduit) {
		case TYPE_WEED_DAO_PSQL:
			produitA = new WeedDaoPSQL();
			break;
		
		default:
			throw new IllegalArgumentException("Type de produit inconnu");
		}

		return produitA;
	}
}
