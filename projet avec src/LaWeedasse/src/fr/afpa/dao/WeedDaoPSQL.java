package fr.afpa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.afpa.dao.interfaces.IWeedDao;
import fr.afpa.entities.Weed;

public class WeedDaoPSQL implements IWeedDao {
	@Override
	public List<Weed> getAllWeed() {

		List<Weed> weeds = new ArrayList<Weed>();
		Connection connexion = ConnectionPostgreSQL.getInstance();
		PreparedStatement statement = null;
		ResultSet resultat = null;
		try {
			statement = connexion.prepareStatement("SELECT id,nom,prix,poid FROM weeds");
			resultat = statement.executeQuery();

			while (resultat.next()) {
				Integer id = resultat.getInt("id");
				String nom = resultat.getString("nom");
				Integer prix = resultat.getInt("prix");
				Float poid = resultat.getFloat("poid");
				Weed weed = new Weed();
				weed.setId(id);
				weed.setNom(nom);
				weed.setPrix(prix);
				weed.setPoid(poid);
				weeds.add(weed);

			}
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			try {
				if (resultat != null) {
					resultat.close();
				}
				if (statement != null) {
					statement.close();
				}

			} catch (SQLException ignore) {

			}
		}

		return weeds;
	}

	@Override
	public Weed chercherUneWeed(Integer id) {
		Connection connexion = ConnectionPostgreSQL.getInstance();
		PreparedStatement statement = null;
		ResultSet resultat = null;
		try {
			statement = connexion.prepareStatement("SELECT id,nom,prix,poid FROM weeds where id=?");
			statement.setInt(1, id);
			resultat = statement.executeQuery();

			while (resultat.next()) {
				Integer idWeed = resultat.getInt("id");
				String nom = resultat.getString("nom");
				Integer prix = resultat.getInt("prix");
				Float poid = resultat.getFloat("poid");
				Weed weed = new Weed();
				weed.setId(idWeed);
				weed.setNom(nom);
				weed.setPrix(prix);
				weed.setPoid(poid);

				return weed;

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (resultat != null) {
					resultat.close();
				}
				if (statement != null) {
					statement.close();
				}

			} catch (SQLException ignore) {

			}
		}
		return null;
	}

	@Override
	public void addWeed(Weed weed) {
		Connection connexion = ConnectionPostgreSQL.getInstance();
		PreparedStatement statement = null;
		ResultSet resultat = null;
		try {

			statement = connexion.prepareStatement("INSERT INTO weeds (nom,prix,poid) VALUES(?,?,?)");
			statement.setString(1, weed.getNom());
			statement.setInt(2, weed.getPrix());
			statement.setFloat(3, weed.getPoid());

			int nbrLigne = statement.executeUpdate();
			System.out.println("nombre de lignes supprimées : " + nbrLigne);
		} catch (SQLException e) {

		} finally {
			try {
				if (resultat != null) {
					resultat.close();
				}
				if (statement != null) {
					statement.close();
				}

			} catch (SQLException ignore) {

			}
		}
	}

	@Override
	public void deleteWeed(Integer id) {
		Connection connexion = ConnectionPostgreSQL.getInstance();
		PreparedStatement statement = null;
		ResultSet resultat = null;
		try {

			statement = connexion.prepareStatement("delete FROM \"weeds\" where id = ?");
			statement.setInt(1, id);

			int nbrLigne = statement.executeUpdate();
			System.out.println("nombre de lignes supprimées : " + nbrLigne);
		} catch (SQLException e) {

		} finally {
			try {
				if (resultat != null) {
					resultat.close();
				}
				if (statement != null) {
					statement.close();
				}

			} catch (SQLException ignore) {

			}
		}
	}

	@Override
	public boolean updateArticle(Weed weed) {
		Connection connexion = ConnectionPostgreSQL.getInstance();
		PreparedStatement statement = null;
		ResultSet resultat = null;
		try {

			statement = connexion.prepareStatement("update weeds set nom=?,prix=?,poid=? where id=?");
			statement.setString(1, weed.getNom());
			statement.setInt(2, weed.getPrix());
			statement.setFloat(3, weed.getPoid());
			statement.setInt(4, weed.getId());

			int nbrLigne = statement.executeUpdate();

			System.out.println("nombre de lignes supprimées : " + nbrLigne);
			if (nbrLigne > 0)
				return true;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (resultat != null) {
					resultat.close();
				}
				if (statement != null) {
					statement.close();
				}

			} catch (SQLException ignore) {

			}
		}
		return false;
	}
}
