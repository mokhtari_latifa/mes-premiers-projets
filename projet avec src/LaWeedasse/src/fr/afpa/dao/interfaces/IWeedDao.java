package fr.afpa.dao.interfaces;

import java.util.List;

import fr.afpa.entities.Weed;

public interface IWeedDao {

	public List<Weed> getAllWeed();

	public Weed chercherUneWeed(Integer id);

	public void addWeed(Weed weed);

	public void deleteWeed(Integer id);

	public boolean updateArticle(Weed weed);
}
