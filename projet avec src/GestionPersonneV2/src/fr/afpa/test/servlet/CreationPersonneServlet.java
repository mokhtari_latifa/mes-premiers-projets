package fr.afpa.test.servlet;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.test.bean.Personne;
import fr.afpa.test.service.PersonneService;

/**
 * Servlet implementation class CreationPersonneServlet
 */
@WebServlet("/CreationPersonneServlet")
public class CreationPersonneServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CreationPersonneServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.getRequestDispatcher("/jsp/nouvelle_personne.jsp").forward(request, response);;
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	
	       String nom = request.getParameter("nom");
	       String prenom= request.getParameter("prenom");
	       String civilite= request.getParameter("civilite");
	       String dateNaissance= request.getParameter("dateNaissance");
	       String adresse= request.getParameter("adresse");
	       String codePostal= request.getParameter("codepostal");
	       String ville= request.getParameter("ville");
	       String numtel= request.getParameter("numtel");
	       String loisir= request.getParameter("loisir");
	       
	       DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	       
	      try {
			Personne p = new Personne((int)(new Date().getTime()/5000), Integer.parseInt(civilite), nom, prenom, format.parse(dateNaissance), adresse, codePostal, ville, numtel, loisir);
			
			PersonneService.addPersonne(p);
			
			response.sendRedirect(request.getContextPath()+"/");
			
	      } catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	       
	       
	}

}
