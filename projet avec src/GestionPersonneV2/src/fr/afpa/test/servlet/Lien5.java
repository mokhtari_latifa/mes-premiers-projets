package fr.afpa.test.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Lien3
 */
@WebServlet("/Lien5")
public class Lien5 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Lien5() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Map<String, String> maMap = new HashMap<String, String>();
		maMap.put("cle1", "value1");
		maMap.put("cle2", "value2");
		maMap.put("cle3", "value3");
		maMap.put("cle4", "value4");
		maMap.put("cle5", "value5");
		maMap.put("cle6", "value6");
		request.setAttribute("maMap", maMap);
		String[] tab = {"val1","val2","val3","val4","val5"};
		request.setAttribute("monTableau", tab);
		

		request.getRequestDispatcher("/jsp/selector.jsp").forward(request, response);;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
