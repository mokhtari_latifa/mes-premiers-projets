package fr.afpa.test.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.test.bean.Personne;
import fr.afpa.test.service.PersonneService;

/**
 * Servlet implementation class PersonneServlet
 */
@WebServlet(description = "une description", urlPatterns = { "/PersonneServlet", "/index.html" }, initParams = {
		@WebInitParam(name = "var", value = "une valeur", description = "ma description param"),
		@WebInitParam(name = "var2", value = "une valeur3", description = "ma description param2") })
public class PersonneServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PersonneServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		super.destroy();
		System.out.println("Je suis dans destroy de PersonneServlet");
	}

	@Override
	public void init() throws ServletException {
		super.init();
		System.out.println("Je suis dans le initi de PersonneServlet");
	}

	@Override
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		super.init(config);
		System.out.println("Je suis dans le initi de PersonneServlet avec param");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("doGet : debut");
		
		HttpSession session = request.getSession();
		List<Personne> personnes = PersonneService.getAllPersonnes();
		request.setAttribute("toto", "OK");
		request.setAttribute("personnes", personnes);
		request.setAttribute("jours", "Lundi, Mardi, Mercredi,Jeudi, vendredi, samedi,dimanche");
		request.getRequestDispatcher("/jsp/personnes.jsp").forward(request, response);
		System.out.println("doGet : fin");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("doPOST : debut");
		request.setAttribute("toto", "OK");
		//doGet(request, response);
		System.out.println("doPOST : debut");
	}

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("service HttpServletRequest : debut");
		super.service(req, resp);
		System.out.println("service HttpServletRequest : fin");
	}

	@Override
	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("service ServletRequest : debut");
		super.service(req, res);
		System.out.println("service ServletRequest : fin");
	}
	
	

}
