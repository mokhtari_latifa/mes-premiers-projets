package fr.afpa.test.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.afpa.test.bean.Personne;

public class PersonneService {

	private static List<Personne> personnes;

	static {
		personnes = new ArrayList<Personne>();
		personnes.add(
				new Personne(1, 1, "Dubois", "Bernard", new Date(), "adresse1", "59870", "Lille", "0632567", "marche"));
		personnes.add(
				new Personne(2, 1, "Thomas", "Adam", new Date(), "adresse2", "75000", "Paris", "0632265", "sport"));
		personnes.add(
				new Personne(3, 2, "Martin", "Arthur", new Date(), "adresse3", "59370", "Mons", "06325", "Bricolage"));

	}

	public static List<Personne> getAllPersonnes() {
		return personnes;
	}

	public static Personne chercherUnePersonne(Integer id) {
		for (Personne personne : personnes) {
			if (id == personne.getId())
				return personne;
		}
		return null;
	}

	public static void addPersonne(Personne personne) {
		personnes.add(personne);
	}

	public static void deletePersonne(Personne personne) {
		personnes.remove(personne);
	}

	public static void deletePersonne(Integer id) {
		for (Personne personne : personnes) {
			if (id == personne.getId()) {
				personnes.remove(personne);
				break;
			}
		}

	}
}