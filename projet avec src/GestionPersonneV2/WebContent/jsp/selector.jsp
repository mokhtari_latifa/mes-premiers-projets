<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>


<h1>Afficher les elements d'une maps par cle </h1>

<p>  ${ requestScope.maMap["cle1"] } |  ${ requestScope.maMap["cle3"] } </p>


<h1>Afficher les elements d'une maps en utilisant le . </h1>
<p>  ${ requestScope.maMap.cle1 } |  ${ requestScope.maMap.cle3 } </p>
<h1>Afficher les elements d'un tableau </h1>
<p>  ${ requestScope.monTableau[0] } |  ${ requestScope.monTableau[3] } </p>
</body>
</html>