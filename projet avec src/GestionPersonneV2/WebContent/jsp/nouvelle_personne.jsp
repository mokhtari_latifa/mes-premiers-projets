<%@ page language="java" contentType="text/html; charset=UTF8"
	pageEncoding="UTF8"%>
<!DOCTYPE html>
<html lang="fr">
​
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
​
<body>
    <form method="POST" action="<%=request.getContextPath()%>/CreationPersonneServlet">
        <label for="nom">Nom2 :</label>
        <input type="text" name="nom" id="" /><br>
        <label for="prenom">Prenom :</label>
        <input type="text" name="prenom" id="" /><br>
        <label for="">Civilité</label>
        <input type="radio" name="civilite" value ="1">
        <label for="civilite">Mr.</label>
        <input type="radio" name="civilite" value ="2">
        <label for="civilite">Mme.</label><br>
        <label for="dateNaissance">Date de naissance :</label>
        <input type="date" name="dateNaissance" id=""><br>
        <label for="adresse">Adresse :</label>
        <input type="text" name="adresse" id="" /><br>
        <label for="codepostal">Code postal :</label>
        <input type="number" name="codepostal" /><br>
        <label for="ville">Ville :</label>
        <input type="text" name="ville" id="" /><br>
        <label for="numtel">Numéro de téléphone :</label>
        <input type="tel" name="numtel" id="" /><br>
        <label for="loisir">Loisir :</label>
        <select name="loisir" size="1">
            <option value="voyage">Voyage</option>
            <option value="sport">Sport</option>
            <option value="jeux">Jeux-vidéos</option>
        </select> <br>
        <button type="submit">Créer une personne</button>
    </form>
</body>
​
</html>