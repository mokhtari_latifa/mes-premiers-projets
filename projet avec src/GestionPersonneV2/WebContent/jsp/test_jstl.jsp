<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<jsp:useBean id="p" class="fr.afpa.test.bean.Personne"></jsp:useBean>
	<jsp:setProperty name="p" property="nom" value="toto" />

	<c:if test="${ 5 > 2 }">

		<h1>${ p.nom eq 'toto' }</h1>
		<h1>${ requestScope.personne1.nom }</h1> ou <h1>${ requestScope["personne1"].nom }</h1>
		<h1>${paramValues["params"][0]}</h1> ou <h1>${paramValues.params[0]}</h1>

	</c:if>


</body>
</html>