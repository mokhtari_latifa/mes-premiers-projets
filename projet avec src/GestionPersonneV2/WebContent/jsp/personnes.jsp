<%@page import="java.util.ArrayList"%>
<%@page import="fr.afpa.test.bean.Personne"%>
<%@page import="java.util.List"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
	<h1>Bienvenue sur notre super annuaire !</h1>
	<a href="<%=request.getContextPath()%>/CreationPersonneServlet">Ajouter
		Une Personne</a>
	<h2>Voici les personnes :</h2>
	<div>
		<!-- 	table 1 avec if -->
		<table class="table">
			<thead class="grey lighten-2">
				<tr>
					<th scope="col">#</th>
					<th scope="col">Personne</th>
					<th scope="col">Modification</th>
					<th scope="col">Suppression</th>
				</tr>
			</thead>
			<tbody>
				<c:if test="${empty requestScope.personnes  }">
			
			"Pas de résultat" 
			
			</c:if>
				<c:if test="${not empty requestScope.personnes }">
					<c:forEach var="personne" items="${requestScope.personnes}">
						<tr>
							<th scope="row">${personne.id }</th>
							<td><a href="details.html?id=${personne.id }">${personne.nom}</a></td>
							<td><a href="modifier.html?id=${personne.id }"><img
									class="tools" src="img/Modifier.png" alt="Modifier"></a></td>
							<td><a href="SupprimerPersonneServlet?id=${personne.id }"><img
									class="tools" src="img/suppression.png" alt="Supprimer"></a></td>
						</tr>

					</c:forEach>

				</c:if>





			</tbody>
		</table>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<hr>
		<!-- table 2 avec choose -->
		<table class="table">
			<thead class="grey lighten-2">
				<tr>
					<th scope="col">#</th>
					<th scope="col">Personne</th>
					<th scope="col">Modification</th>
					<th scope="col">Suppression</th>
				</tr>
			</thead>
			<tbody>






				<c:choose>
					<c:when test="${empty requestScope.personnes  }">
		"Pas de résultat" 
		</c:when>
					<c:otherwise>
						<c:forEach var="personne" items="${requestScope.personnes}"  >
							<tr>
							
								<th scope="row">${ personne.id }</th>
								<td><a href="details.html?id=${personne.id }">${personne.nom}</a></td>
								<td><a href="modifier.html?id=${personne.id }"><img
										class="tools" src="img/Modifier.png" alt="Modifier"></a></td>
								<td><a href="SupprimerPersonneServlet?id=${personne.id }"><img
										class="tools" src="img/suppression.png" alt="Supprimer"></a></td>
							</tr>

						</c:forEach>
						
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
<ul>
			<c:forTokens var="val" items="${ requestScope.jours }" delims=",">

				<li>${ val }</li>
			</c:forTokens>
		</ul>
		
		<ul>
			<c:forTokens var="val" items="toto,tata,titi,fr" delims=",">

				<li>${ val }</li>
			</c:forTokens>
		</ul>
		<ul>
			<c:forEach var="i"  step="1" begin="0" end="${requestScope.personnes[1].id + 3}" >
				<li>${ i }</li>
			</c:forEach>
		</ul>
		
		
	</div>
</body>
</html>