<%@page import="fr.afpa.test.bean.Personne"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%-- 		<% Personne pJ = new Personne(); --%>
	// pJ.setId(12);
	<%-- 		pJ.setAdresse("adresse");%> --%>
	<!-- Class ==> creation nouveau Objet : type ==> recuperation d'un objet -->
	<jsp:useBean id="p" class="fr.afpa.test.bean.Personne" scope="page"></jsp:useBean>
	<jsp:setProperty name="p" property="adresse" value="UneAdresse" />

	<%-- 	<%= p.getAdresse() %> --%>
	<h1><jsp:getProperty name="p" property="adresse" /></h1>


	<!-- recuperation des valeurs session, request et application -->
	<!-- Class ==> creation nouveau Objet : type ==> recuperation d'un objet -->
	<%-- <% String messageRequest = (String) request.getAttribute("messageRequest"); %> --%>
	<jsp:useBean id="messageRequest" scope="request"
		type="java.lang.String"></jsp:useBean>
	<%-- <% String messageRequest = (String) request.getSession().getAttribute("messageRequest"); %> --%>
	<jsp:useBean id="messageSession" scope="session"
		type="java.lang.String"></jsp:useBean>
	<%-- <% String messageRequest = (String) request.getServletContext().getAttribute("messageRequest"); %> --%>
	<jsp:useBean id="messageApplication" scope="application"
		type="java.lang.String"></jsp:useBean>

	<h2><%=messageRequest%></h2>
	<h2><%=messageSession%></h2>
	<h2><%=messageApplication%></h2>
</body>
</html>