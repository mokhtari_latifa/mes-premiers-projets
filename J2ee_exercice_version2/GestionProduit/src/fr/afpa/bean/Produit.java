package fr.afpa.bean;

public class Produit {
	private Integer id;
	private String nom;
	private Double prix;
	private String model ;
	private String marque;
	private String couleur;
	private String description;
	private String unite ;
    private String lhp ;
    private String categorie;

	public Produit() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	public Produit(Integer id, String nom, Double prix, String model, String marque, String couleur, String description,
			String unite, String lhp, String categorie) {
		super();
		this.id = id;
		this.nom = nom;
		this.prix = prix;
		this.model = model;
		this.marque = marque;
		this.couleur = couleur;
		this.description = description;
		this.unite = unite;
		this.lhp = lhp;
		this.categorie = categorie;
	}









	/**
	 * @return the lhp
	 */
	public String getLhp() {
		return lhp;
	}



	/**
	 * @param lhp the lhp to set
	 */
	public void setLhp(String lhp) {
		this.lhp = lhp;
	}



	/**
	 * @return the unite
	 */
	public String getUnite() {
		return unite;
	}




	/**
	 * @param unite the unite to set
	 */
	public void setUnite(String unite) {
		this.unite = unite;
	}









	/**
	 * @return the categorie
	 */
	public String getCategorie() {
		return categorie;
	}




	/**
	 * @param categorie the categorie to set
	 */
	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}




	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the prix
	 */
	public Double getPrix() {
		return prix;
	}
	/**
	 * @param prix the prix to set
	 */
	public void setPrix(Double prix) {
		this.prix = prix;
	}
	/**
	 * @return the model
	 */
	public String getModel() {
		return model;
	}
	/**
	 * @param model the model to set
	 */
	public void setModel(String model) {
		this.model = model;
	}
	/**
	 * @return the marque
	 */
	public String getMarque() {
		return marque;
	}
	/**
	 * @param marque the marque to set
	 */
	public void setMarque(String marque) {
		this.marque = marque;
	}



	/**
	 * @return the couleur
	 */
	public String getCouleur() {
		return couleur;
	}



	/**
	 * @param couleur the couleur to set
	 */
	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}



	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}



	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}







	

	
	
	

}
