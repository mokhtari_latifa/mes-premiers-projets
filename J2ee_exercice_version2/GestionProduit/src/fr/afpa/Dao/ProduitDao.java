/**
 * 
 */
package fr.afpa.Dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import fr.afpa.bean.Produit;
import fr.afpa.configuration.ConnexionFactory;

/**
 * @author elyla
 *
 */
public class ProduitDao {

	/**
	 * Creation d'un produit
	 * @param produit
	 */
	public void createProduct(Produit produit) {
		PreparedStatement pStatement;
		Integer id = getLastId();
		id++;
		produit.setId(id);
		try {
			pStatement = ConnexionFactory.getConnection().prepareStatement("insert into produit Values (?,?,?,?,?,?,?,?,?,?)");
			pStatement.setInt(1, produit.getId());
			pStatement.setString(2, produit.getNom());
			pStatement.setDouble(3, produit.getPrix());
			pStatement.setString(4, produit.getModel());
			pStatement.setString(5, produit.getMarque());
			pStatement.setString(5, produit.getCouleur());
			pStatement.setString(5, produit.getDescription());
			pStatement.setString(5, produit.getUnite());
			pStatement.setString(5, produit.getLhp());
			pStatement.setString(5, produit.getCategorie());
			int row = pStatement.executeUpdate();
			System.out.println(row);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * get last id from BDD
	 */
	public Integer getLastId() {
		Integer idmax = null;

		PreparedStatement pStatement;
		try {
			pStatement = ConnexionFactory.getConnection().prepareStatement("select max(id ) from produit");
			ResultSet monResultat = pStatement.executeQuery();
			while (monResultat.next()) {
				idmax = monResultat.getInt(1);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return idmax;
	}

	
//	public static Produit getByL(String login , String passeWord) {
//		PreparedStatement pStatement;
//		Joueur joueur = null;
//		try {
//			pStatement = ConnexionFactory.getConnection().prepareStatement("SELECT * from joueur where login = ? and password = ?");
//			pStatement.setString(1, login);
//			pStatement.setString(2, passeWord);
//
//			ResultSet monResultat = pStatement.executeQuery();
//			while (monResultat.next()) {
//				joueur = new Joueur();
//				joueur.setId(monResultat.getInt("id"));
//				joueur.setNom(monResultat.getString("nom"));
//				joueur.setPrenom(monResultat.getString("prenom"));
//				joueur.setLogin(monResultat.getString("login"));
//				joueur.setMotDePasse(monResultat.getString("password"));
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//
//		return joueur;
//	}


}
