package fr.afpa.service;

import java.util.ArrayList;
import java.util.List;

import fr.afpa.bean.Produit;

public class ProduitService {

	private static List<Produit> produits;

	static {
		produits = new ArrayList<Produit>();
		produits.add(new Produit(1, "produit1", 20d, "model1", "marque1", "bleu", "description 1", "kg", "20,30,10",
				"categorie1"));
		produits.add(new Produit(2, "produit2", 18d, "model2", "marque2", "rouge", "description 2", "mm", "20,30,10",
				"categorie1"));
		produits.add(new Produit(3, "produit3", 200d, "model3", "marque3", "noir", "description 3", "kg", "10,40,50",
				"categorie1"));
		produits.add(new Produit(4, "produit4", 2d, "model4", "marque4", "noir", "description 4", "kg", "5,5,8",
				"categorie1"));

	}

	public static List<Produit> getProduits() {
		return produits;
	}

	public static Produit chercherUnProduit(Integer id) {
		for (Produit produit : produits) {
			if (id == produit.getId())
				return produit;
		}
		return null;
	}

	public static void addProduit(Produit p) {
		produits.add(p);
	}

	public static void supprimeProduit(Integer id) {
		for (Produit produit : produits) {
			if (id == produit.getId()) {
				produits.remove(produit);
				break;
			}
			
		}
	}
}
