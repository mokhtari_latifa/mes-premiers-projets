package fr.afpa.configuration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnexionFactory {

	private static Connection connection;

	public static Connection getConnection() {

		if (ConnexionFactory.connection == null) {
			String url = "jdbc:postgresql://vps734804.ovh.net:5432/groupe2_bdd";
			try {
				Class.forName("org.postgresql.Driver");

				Properties props = new Properties();
				props.setProperty("user", "groupe2_user");
				props.setProperty("password", "groupe2");
				ConnexionFactory.connection = DriverManager.getConnection(url, props);
				return connection;
			} catch (ClassNotFoundException e) {
				System.out.println("Impossible de se charger le driver");
				System.exit(-1);
			} catch (SQLException e) {
				System.out.println("Impossible de se connecter à l’url : " + url);
				System.exit(-1);
			}

		}

		return connection;
	}

}
