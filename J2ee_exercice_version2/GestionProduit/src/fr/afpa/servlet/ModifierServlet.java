package fr.afpa.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.bean.Produit;
import fr.afpa.service.ProduitService;

/**
 * Servlet implementation class ModifierServlet
 */
@WebServlet("/ModifierServlet")
public class ModifierServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModifierServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String idString = request.getParameter("id");
		Integer id = Integer.parseInt(idString);
		Produit produit = ProduitService.chercherUnProduit(id);
		request.setAttribute("produit", produit);
		request.getRequestDispatcher("/jsp/ModifierProduit.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		     String nom = request.getParameter("nom");
			 String  prix= request.getParameter("prix");
			 String marque= request.getParameter("marque");
			 String unite= request.getParameter("unite");
			 String model= request.getParameter("model"); 
			 String couleur= request.getParameter("couleur");
			String lhp= request.getParameter("LHP");
			String categorie= request.getParameter("categorie");
			 String description= request.getParameter("description");
			
			 Double prix2 = Double.parseDouble(prix);
			 Integer idProduit = Integer.parseInt(id);
			 
			 Produit produit = ProduitService.chercherUnProduit(idProduit);
			 produit.setNom(nom);
			 produit.setPrix(Double.parseDouble(prix));
			 produit.setMarque(marque);
			 produit.setUnite(unite);
			 produit.setModel(model);
			 produit.setCouleur(couleur);
			 produit.setLhp(lhp);
			 produit.setCategorie(categorie);
			 produit.setDescription(description);
			 
		     
				response.sendRedirect(request.getContextPath()+"/IndexServlet");
	}

}
