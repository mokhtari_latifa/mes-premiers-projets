<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@page import="fr.afpa.bean.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Modifier Produit</title>
 <link rel="stylesheet" href="css/ajouter.css">
</head>
<body>
  <header>
	
        <img src="img/photoheader.jpg">
        </header>
        <nav>
        <div>
        <ul>
            <li><a href="Premiere page.html">Accueil</a></li>
            <li><a href="">Panier</a></li>
            <li><a href="">Nous Contacter</a></li>
            <li><a href="">Qui sommes nous ?</a></li>
        </ul>
        </nav>
      </header>
<% Produit produit = null ;

produit = (Produit)request.getAttribute("produit"); %>
<%-- <%= "Pas de résultat" %> --%>
<%-- <% } else{  %> --%>


<section>

    
    <form action="<%=request.getContextPath()%>/ModifierServlet" method="POST" class="form-example">
  <fieldset>
      <legend>MODIFIER LE PRODUIT : </legend>
      <div class="form-example">
          <label for="id"> ID: </label>
          <input type="text" name="id" value="<%= produit.getId() %>"  readonly="readonly">
        </div>
      <div class="form-example">
          <label for="name"> Nom: </label>
          <input type="text" name="nom" value="<%= produit.getNom() %>" required>
        </div>
      
        <div class="form-example">
          <label for="prix">Prix: </label>
          <input type="number" name="prix" value="<%= produit.getPrix() %>" required>
        </div>
        <div class="form-example">
            <label for="marque">Marque: </label>
            <input type="text" name="marque" value="<%= produit.getMarque() %>"  required>
          </div>
          <div class="form-example">
            <label for="unite">Unité: </label>
            <input type="text" name="unite" value="<%= produit.getUnite() %>"  required>
          </div>
          <div class="form-example">
            <label for="model">Model: </label>
            <input type="text" name="model" value="<%= produit.getModel() %>"   required>
          </div>
          <div class="form-example">
            <label for="couleur">Couleur: </label>
            <input type="color" name="couleur" value="<%= produit.getCouleur() %>"  required>
          </div>
          <div class="form-example">
            <label for="lhp">LHP: </label>
            <input type="text" name="lhp"  value="<%= produit.getLhp() %>" required>
          </div>
          <div class="form-example">
          <select name="categorie" value="<%= produit.getCategorie() %>" selected >
            <option value="categorie1" <%="categorie1".equals(produit.getCategorie())?"selected":"" %>>Catégorie 1</option>
            <option value="categorie2" <%="categorie2".equals(produit.getCategorie())?"selected":"" %>>Catégorie 2</option>
            <option value="categorie3" <%="categorie3".equals(produit.getCategorie())?"selected":"" %>>Catégorie 3</option>
            <option value="categorie4" <%="categorie4".equals(produit.getCategorie())?"selected":"" %>>Catégorie 4</option>
          </select>
          </div>
          <br>
          <div class="form-example">
          <textarea rows="4" cols="50" name="description"  > <%= produit.getDescription() %>.</textarea>
        </div>
        <br>

        <div class="button">
          <button type="submit">Modifier le produit</button>
        </div>
    </fieldset>
      </form>
    </section>
</body>
</html>