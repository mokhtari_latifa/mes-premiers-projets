<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
    <link rel="stylesheet" href="css/ajouter.css">
    <title>Ajouter UN produit</title>
</head>
<body>
   <header>
	
        <img src="img/photoheader.jpg">
        </header>
        <nav>
        <div>
        <ul>
            <li><a href="Premiere page.html">Accueil</a></li>
            <li><a href="">Panier</a></li>
            <li><a href="">Nous Contacter</a></li>
            <li><a href="">Qui sommes nous ?</a></li>
        </ul>
        </nav>
      </header>
<section>
    
    <form action="<%=request.getContextPath()%>/AjouterServlet" method="POST" class="form-example">
  <fieldset>
      <legend>AJOUTER UN PRODUIT</legend>
      <div class="form-example">
          <label for="name"> Nom: </label>
          <input type="text" name="nom" required>
        </div>
      
        <div class="form-example">
          <label for="prix">Prix: </label>
          <input type="number" name="prix" required>
        </div>
        <div class="form-example">
            <label for="marque">Marque: </label>
            <input type="text" name="marque" required>
          </div>
          <div class="form-example">
            <label for="unite">Unit�: </label>
            <input type="text" name="unite"  required>
          </div>
          <div class="form-example">
            <label for="model">Model: </label>
            <input type="text" name="model"  required>
          </div>
          <div class="form-example">
            <label for="couleur">Couleur: </label>
            <input type="color" name="couleur" required>
          </div>
          <div class="form-example">
            <label for="lhp">LHP: </label>
            <input type="text" name="lhp"  required>
          </div>
          <div class="form-example">
          <select name="categorie">
            <option value="categorie1">Cat�gorie1</option>
            <option value="categorie2">Cat�gorie2</option>
            <option value="categorie3">Cat�gorie3</option>
            <option value="categorie4">Cat�gorie4</option>
          </select>
          </div>
          <br>
          <div class="form-example">
          <textarea rows="4" cols="50" name="description" >
            Description...</textarea>
        </div>
        <br>

        <div class="button">
          <button type="submit">Cr�er un produit</button>
        </div>
    </fieldset>
      </form>
    </section>
</body>
</html>