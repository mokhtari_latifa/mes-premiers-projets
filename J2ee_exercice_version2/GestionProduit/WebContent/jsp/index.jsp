<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="java.util.List"%>
    <%@page import="fr.afpa.bean.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="css/ajouter.css">
</head>
<body>

   <header>
	
        <img src="img/photoheader.jpg">
        </header>
        <nav>
        <div>
        <ul>
            <li><a href="Premiere page.html">Accueil</a></li>
            <li><a href="">Panier</a></li>
            <li><a href="">Nous Contacter</a></li>
            <li><a href="">Qui sommes nous ?</a></li>
        </ul>
        </nav>
      </header>
 <h1>Liste des produits</h1>


<div>
		<table class="table">
			<thead class="grey lighten-2">
				<tr>
					<th scope="col">ID</th>
					<th scope="col">Nom</th>
					<th scope="col">Prix</th>
					<th scope="col">Marque</th>
					
				</tr>
			</thead>
			<tbody>
			
			<% List<Produit> produits = null;
			produits = (List<Produit>) request.getAttribute("produits");
			if(produits==null || produits.isEmpty()){%>
				<%= "Pas de r�sultat" %>
			<%}else{
					for(Produit produit : produits){ %>
						<tr>
							<th scope="row"><%=produit.getId() %></th>
								<td><a href="<%= request.getContextPath()%>/DetailServlet?id=<%=produit.getId()%>"><%=produit.getNom() %></a></td>
								<td><a href="<%= request.getContextPath()%>/DetailServlet?id=<%=produit.getId()%>"><%=produit.getPrix() %></a></td>
								<td><a href="<%= request.getContextPath()%>/DetailServlet?id=<%=produit.getId()%>"><%=produit.getMarque() %></a></td>
								<td><a href="<%= request.getContextPath()%>/ModifierServlet?id=<%=produit.getId()%>">MODIFIER</a></td>
								<td><a href="<%= request.getContextPath()%>/SupprimeServlet?id=<%=produit.getId()%>">SUPPRIMER</a></td>
						</tr>
					<%} 
			}%>
			</tbody>
		</table>
	</div>




 <button ><a href="<%= request.getContextPath()%>/AjouterServlet"> Ajouter un produit</a></button>
</body>
</html>